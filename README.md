# 3D Animation Controls based on Body Motions

This project attempts to pose and animate a 3D character model using the physical hand movements of the animator.
Posing directly with the hand offers the following benefit:

* The artist can directly synchronize her physical hand movements with the character hand.
  This makes character posing more intuitive and faster.
		

## Technology Used

	* 	Unity3D: Unity was choosen as a base framework for implementing the posing functionality.
	* 	Manus VR: Sensor glove used to accurately capture finger movements of the artist. 
	*	Leap Motion: Used to track the global position of the hand.
	








