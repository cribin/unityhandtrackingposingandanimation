﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools.Utils;

public class DeepHandTest {

    [Test]
    public void DeepHandTestSimplePasses() {
       

        //ARRANGE
        int numOfFingers = 5;
        int numOfJointsPerFinger = 3;
        int dofFingerJointPosition = 3;
        int dofFingerJointRotation = 4;

        DeepHand deepHand = new DeepHand(numOfJointsPerFinger, dofFingerJointPosition, dofFingerJointRotation);

        Quaternion[] fingerJointRotations = CreateRandomFingerJointRotations(numOfJointsPerFinger * numOfFingers);

        float[] floatFingerJointRotations = TransformRandomQuaternionsToFloatArray(fingerJointRotations);
        
        //ACT
        deepHand.SetFingerJointsRotation(floatFingerJointRotations);

        //ASSERT
        QuaternionEqualityComparer quaternionEquality = new QuaternionEqualityComparer(0.01f);
       
        for (int i = 0; i < numOfFingers; i++)
        {
            for (int j = 0; j < numOfJointsPerFinger; j++)
            {
                Debug.Log("Expected: " + fingerJointRotations[j + i * numOfJointsPerFinger]);
                Debug.Log("Actual: " + deepHand.GetFingerJointsRotation((FingerType)i)[j]);
                Assert.True(quaternionEquality.Equals(fingerJointRotations[ j + i*numOfJointsPerFinger], deepHand.GetFingerJointsRotation((FingerType)i)[j]));
            }
            
        }
    }

    private Quaternion[] CreateRandomFingerJointRotations(int totalJoints)
    {
       
        Quaternion[] fingerJointRotations = new Quaternion[totalJoints];
        for (int i = 0; i < totalJoints; i++)
        {
            fingerJointRotations[i] = Random.rotation;
        }

        return fingerJointRotations;
    }

    private float[] TransformRandomQuaternionsToFloatArray(Quaternion[] fingerJointRotations)
    {
        int totalJoints = fingerJointRotations.Length;
        int dofQuaternion = 4;
        float[] allFingerJointRotations = new float[totalJoints * dofQuaternion];
        for (int i = 0; i < totalJoints; i++)
        {
            int offset = i * dofQuaternion;
            for(int j =  0; j < dofQuaternion; j++)
                allFingerJointRotations[j + offset] = fingerJointRotations[i][j];
        }

        return allFingerJointRotations;
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator DeepHandTestWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
}
