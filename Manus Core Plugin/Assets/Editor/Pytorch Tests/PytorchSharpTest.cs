﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PytorchSharpTest {

    [Test]
    public void PytorchSharpReceivesCorrectInput() {

        //ARRANGE
        string standardError;
        string expectedOutputText = "1";

        string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";

        string pythonScriptPath = "C:\\Users\\chalu\\PycharmProjects\\pyTorchHelloWorld\\hello_world.py";

        PytorchSharpPython pytorchSharp = new PytorchSharpPython(filePythonExePath);

        //ACT
        string acutalOutputText = pytorchSharp.ExecutePythonScript(pythonScriptPath, out standardError);
        
        
        //ASSERT

        Assert.AreEqual(expectedOutputText, acutalOutputText);
    }

    [Test]
    public void PytorchSharpZeroTensorTest()
    {

        //ARRANGE
        string standardError;
        string expectedOutputText = "tensor";

        string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";

        string pythonScriptPath = "C:\\Users\\chalu\\PycharmProjects\\pyTorchHelloWorld\\tensor_print.py";

        PytorchSharpPython pytorchSharp = new PytorchSharpPython(filePythonExePath);

        //ACT
        string acutalOutputText = pytorchSharp.ExecutePythonScript(pythonScriptPath, out standardError);


        //ASSERT

        Assert.AreEqual(expectedOutputText, acutalOutputText);
    }

    [Test]
    public void PytorchSharpDeepHandTest()
    {

        //ARRANGE
        string standardError;
        string expectedOutputText = "torch.Size([1, 22, 368, 368])";

        string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";

        string pythonScriptPath = "C:\\Users\\chalu\\OneDrive\\Documents\\PrashanthHandPoseEstimationSource\\handPoseEstimation-master\\src\\UnitTests\\Tests.py";

        PytorchSharpPython pytorchSharp = new PytorchSharpPython(filePythonExePath);

        //ACT
        string acutalOutputText = pytorchSharp.ExecutePythonScript(pythonScriptPath, out standardError);
        Debug.Log("StandardError: " + standardError);
        //ASSERT

        Assert.AreEqual(expectedOutputText, acutalOutputText);
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator NewTestScriptWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
}
