﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools.Utils;

public class PytorchDeepHandsManagerTest
{
    [Test]
    public void PytorchDeepHandsManagerTestSimplePasses()
    {
        //ARRANGE

        string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";

        string pythonScriptPath =
            "C:\\Users\\chalu\\OneDrive\\Documents\\PrashanthHandPoseEstimationSource\\handPoseEstimation-master\\src\\UnitTests\\Tests.py";

        char _delimiter = ' ';
        int _numOfData = 17 * 4;

        int numOfFingers = 5;
        int numOfJointsPerFinger = 3;
        int dofFingerJointPosition = 3;
        int dofFingerJointRotation = 4;

        ISharpPython sharpPython = new PytorchSharpPython(filePythonExePath);
        IPythonScriptOutputParser pythonScriptOutputParser = new PytorchHandScriptOutputParser(_delimiter, _numOfData);

        var deepHand = new DeepHand(numOfJointsPerFinger, dofFingerJointPosition, dofFingerJointRotation);

        //ACT
         
        //Call script
        string standardError;
        string pytorchOutput = sharpPython.ExecutePythonScript(pythonScriptPath, out standardError);

        //Parse output
        float[] pyTorchFloatOutput = pythonScriptOutputParser.ParsePythonStringOutput(pytorchOutput);

        //set output to deep hands

        float[] pyTorchFingerOutput = new float[pyTorchFloatOutput.Length - 8];
        Array.Copy(pyTorchFloatOutput, 8, pyTorchFingerOutput, 0, pyTorchFingerOutput.Length);


        deepHand.SetFingerJointsRotation(pyTorchFingerOutput);
        Quaternion[] actualFingerJointRotations = deepHand.GetAllFingerJointsRotations();

        //ASSERT

        /* Debug.Log(pytorchOutput);
            string rotationsString = "";
            foreach (var item in actualFingerJointRotations)
            {
                rotationsString += item.x + " " + item.y + " " + item.z + " " + item.w + " ";
            }
            Debug.Log(rotationsString);*/

        /* QuaternionEqualityComparer quaternionEquality = new QuaternionEqualityComparer(0.01f);

         for (int i = 0; i < numOfFingers; i++)
         {
             for (int j = 0; j < numOfJointsPerFinger; j++)
             {
                 Debug.Log("Expected: " + fingerJointRotations[j + i * numOfJointsPerFinger]);
                 Debug.Log("Actual: " + deepHand.GetFingerJointsRotation((FingerType)i)[j]);
                 Assert.True(quaternionEquality.Equals(fingerJointRotations[j + i * numOfJointsPerFinger], deepHand.GetFingerJointsRotation((FingerType)i)[j]));
             }

         }*/

        Assert.True(actualFingerJointRotations != null);
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator PytorchDeepHandsManagerTestWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
}