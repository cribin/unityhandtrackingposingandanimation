﻿using System;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Globalization;

public class PytorchHandOutputParserTest {

    [Test]
    public void PytorchHandOutputParserTestSimplePasses() {
        // Use the Assert class to test conditions.

        //ARRANGE
        string pytorchOutput = "3.923714458942413330e-01 8.134599685668945312e+00 1.008847746998071671e+01 3.174271345138549805e+00 5.033619880676269531e+00 1.022313746809959412e01 6.523359298706054688e+00 -1.439355134963989258e+00 1.090526485443115234e+01 8.892646789550781250e+00 -4.930084228515625000e+00 1.214919519424438477e+01";
    
        char delimiter = ' ';
        int numOfData = 12;
        IPythonScriptOutputParser pytorchHandOutputParser = new PytorchHandScriptOutputParser(delimiter, numOfData);

        string[] expectedOutput = pytorchOutput.Split(new[] {delimiter}, StringSplitOptions.RemoveEmptyEntries);
        float[] expectedOutputFloat = new float[numOfData];
        for (int i = 0; i < numOfData; i++)
        {
            expectedOutputFloat[i] = (float)Double.Parse(expectedOutput[i], NumberStyles.Float, CultureInfo.InvariantCulture);
            Debug.Log(expectedOutputFloat[i]);
        }

        //ACT
        float[] actualOutputFloat = pytorchHandOutputParser.ParsePythonStringOutput(pytorchOutput);
      
        //ASSERT
        CollectionAssert.AreEqual(expectedOutputFloat, actualOutputFloat);
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator PytorchHandOutputParserTestWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
}
