﻿using UnityEngine;
using UnityEditor;
using Assets.Unity_Runtime_Recorder.Scripts.UnityAnimSaver;

[CustomEditor(typeof(MyUnityAnimationRecorder))]
public class MyUnityAnimationRecorderEditor : Editor
{
    SerializedProperty keyFrameTimeStampManager;

    SerializedProperty targetTransform;

    // save file path
    SerializedProperty savePath;

    SerializedProperty fileName;

    // options
    SerializedProperty showLogGUI;

    SerializedProperty maxNumberOfFrames;
    SerializedProperty recordBlendShape;

    SerializedProperty changeTimeScale;
    SerializedProperty timeScaleOnStart;
    SerializedProperty timeScaleOnRecord;


    void OnEnable()
    {
        keyFrameTimeStampManager = serializedObject.FindProperty("_keyFrameTimeStampManager");
        targetTransform = serializedObject.FindProperty("_targetTransform");
        savePath = serializedObject.FindProperty("savePath");
        fileName = serializedObject.FindProperty("fileName");


        showLogGUI = serializedObject.FindProperty("showLogGUI");
        maxNumberOfFrames = serializedObject.FindProperty("maxNumberOfFrames");
        recordBlendShape = serializedObject.FindProperty("recordBlendShape");

        changeTimeScale = serializedObject.FindProperty("changeTimeScale");
        timeScaleOnStart = serializedObject.FindProperty("timeScaleOnStart");
        timeScaleOnRecord = serializedObject.FindProperty("timeScaleOnRecord");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(keyFrameTimeStampManager);
        EditorGUILayout.PropertyField(targetTransform);

        EditorGUILayout.LabelField("== Path Settings ==");

        if (GUILayout.Button("Set Save Path"))
        {
            string defaultName = serializedObject.targetObject.name + "-Animation";
            string targetPath = EditorUtility.SaveFilePanelInProject("Save Anim File To ..", defaultName, "",
                "please select a folder and enter the file name");

            int lastIndex = targetPath.LastIndexOf("/");
            savePath.stringValue = targetPath.Substring(0, lastIndex + 1);
            string toFileName = targetPath.Substring(lastIndex + 1);

            fileName.stringValue = toFileName;
        }
        EditorGUILayout.PropertyField(savePath);
        EditorGUILayout.PropertyField(fileName);


        EditorGUILayout.Space();

        // keys setting
        EditorGUILayout.LabelField("== Control Keys ==");


        EditorGUILayout.Space();

        // Other Settings
        EditorGUILayout.LabelField("== Other Settings ==");
        recordBlendShape.boolValue = EditorGUILayout.Toggle("Record BlendShapes", recordBlendShape.boolValue);
        bool timeScaleOption = EditorGUILayout.Toggle("Change Time Scale", changeTimeScale.boolValue);
        changeTimeScale.boolValue = timeScaleOption;

        if (timeScaleOption)
        {
            timeScaleOnStart.floatValue = EditorGUILayout.FloatField("TimeScaleOnStart", timeScaleOnStart.floatValue);
            timeScaleOnRecord.floatValue =
                EditorGUILayout.FloatField("TimeScaleOnRecord", timeScaleOnRecord.floatValue);
        }

        // gui log message
        showLogGUI.boolValue = EditorGUILayout.Toggle("Show Debug On GUI", showLogGUI.boolValue);


        EditorGUILayout.PropertyField(maxNumberOfFrames);

        serializedObject.ApplyModifiedProperties();

        //DrawDefaultInspector ();
    }
}