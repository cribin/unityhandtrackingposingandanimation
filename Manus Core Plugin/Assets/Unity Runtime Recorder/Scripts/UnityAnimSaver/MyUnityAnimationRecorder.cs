﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Unity_Runtime_Recorder.Scripts.UnityAnimSaver
{
    public class MyUnityAnimationRecorder : MonoBehaviour
    {
        [SerializeField] private KeyFrameTimeStampManager _keyFrameTimeStampManager;

        [SerializeField] private Transform _targetTransform;

        //Event which is called when a new clip is created
        public event Action<AnimationClip> NewClipCreatedEvent;

        // save file path
        public string savePath;

        public string fileName;

        // use it when save multiple files
        int fileIndex = 0;

        // options
        public bool showLogGUI = false;

        string logMessage = "";

        public int maxNumberOfFrames = 50;
        int frameIndex = 0;

        public bool changeTimeScale = false;
        public float timeScaleOnStart = 0.0f;
        public float timeScaleOnRecord = 1.0f;

        public bool recordBlendShape = false;


        Transform[] recordObjs;
        SkinnedMeshRenderer[] blendShapeObjs;
        MyUnityObjectAnimation[] objRecorders;
        List<UnityBlendShapeAnimation> blendShapeRecorders;

        bool _captureKeyFrame = false;
        float nowTime = 0.0f;

        private float deltaTestTimeSec = 2.0f;

        private AnimationClip _currAnimclip;

        // Use this for initialization
        void Start()
        {
            if (_keyFrameTimeStampManager == null)
            {
                Debug.LogError("MyUnityAnimationRecorder needs a KeyFrameTimeStampManager!");
                return;
            }

            _keyFrameTimeStampManager.NewFrameCapturedEvent += CaptureKeyFrame;
            _keyFrameTimeStampManager.FrameModifiedEvent += ModifyKeyFrame;
            _keyFrameTimeStampManager.FrameDeletedEvent += DeleteKeyFrame;

            SetupRecorders();

            
        }

        void OnDestroy()
        {
            _keyFrameTimeStampManager.NewFrameCapturedEvent -= CaptureKeyFrame;
            _keyFrameTimeStampManager.FrameModifiedEvent -= ModifyKeyFrame;
            _keyFrameTimeStampManager.FrameDeletedEvent -= DeleteKeyFrame;
        }

        void SetupRecorders()
        {
            recordObjs = _targetTransform.gameObject.GetComponentsInChildren<Transform>();
            objRecorders = new MyUnityObjectAnimation[recordObjs.Length];
            blendShapeRecorders = new List<UnityBlendShapeAnimation>();

            frameIndex = 0;
            nowTime = 0.0f;

            for (int i = 0; i < recordObjs.Length; i++)
            {
                string path = AnimationRecorderHelper.GetTransformPathName(_targetTransform, recordObjs[i]);
                objRecorders[i] = new MyUnityObjectAnimation(path, recordObjs[i], maxNumberOfFrames);

                // check if theres blendShape
                if (recordBlendShape)
                {
                    if (recordObjs[i].GetComponent<SkinnedMeshRenderer>())
                    {
                        SkinnedMeshRenderer tempSkinMeshRenderer = recordObjs[i].GetComponent<SkinnedMeshRenderer>();

                        // there is blendShape exist
                        if (tempSkinMeshRenderer.sharedMesh.blendShapeCount > 0)
                        {
                            blendShapeRecorders.Add(new UnityBlendShapeAnimation(path, tempSkinMeshRenderer));
                        }
                    }
                }
            }

            if (changeTimeScale)
                Time.timeScale = timeScaleOnStart;
        }

        // Update is called once per frame
        void Update()
        {
            if (_captureKeyFrame)
            {
                for (int i = 0; i < objRecorders.Length; i++)
                {
                    objRecorders[i].AddFrame(nowTime);
                }

                if (recordBlendShape)
                {
                    for (int i = 0; i < blendShapeRecorders.Count; i++)
                    {
                        blendShapeRecorders[i].AddFrame(nowTime);
                    }
                }

                _captureKeyFrame = false;
            }
        }

        public void CaptureKeyFrame(float captureTimeStamp)
        {
            //CustomDebug("CapturingKeyFrame");
            nowTime = captureTimeStamp;
            _captureKeyFrame = true;
            Time.timeScale = timeScaleOnRecord;
        }

        private void ModifyKeyFrame(int keyFrameIndex, float keyFrameTimeStamp)
        {
            CustomDebug("Modified Keyframe " + keyFrameIndex);
            foreach (MyUnityObjectAnimation objRecorder in objRecorders)
            {
                objRecorder.ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp);
            }

            /* if (!recordBlendShape) return;
             foreach (UnityBlendShapeAnimation t in blendShapeRecorders)
             {
                 t.DeleteLastKeyFrame();
             }*/
        }

        public void DeleteKeyFrame(int keyFrameIndex)
        {
            CustomDebug("Deleting  Keyframe " + keyFrameIndex);


            foreach (MyUnityObjectAnimation objRecorder in objRecorders)
            {
                objRecorder.DeleteKeyFrame(keyFrameIndex);
            }

            if (!recordBlendShape) return;
            foreach (UnityBlendShapeAnimation t in blendShapeRecorders)
            {
                t.DeleteLastKeyFrame();
            }
        }

        public void SaveCurrentRecording()
        {
            CustomDebug("Save current Record, generating .anim file");
            _captureKeyFrame = false;

            ExportAnimationClip();
        }

        void ResetRecorder()
        {
            SetupRecorders();
        }

        void OnGUI()
        {
            if (showLogGUI)
                GUILayout.Label(logMessage);
        }

        void ExportAnimationClip()
        {
            fileIndex++;
            string exportFilePath = savePath + fileName  + ".anim";

            // if record multiple files when run
            /* if (fileIndex != 0)
                 exportFilePath += "-" + fileIndex + ".anim";
             else
                 exportFilePath += ".anim";*/

            _currAnimclip = new AnimationClip
            {
                name = fileName,
                wrapMode = WrapMode.Loop
            };
            AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(_currAnimclip);
            settings.loopTime = true;
            AnimationUtility.SetAnimationClipSettings(_currAnimclip, settings);

            for (int i = 0; i < objRecorders.Length; i++)
            {
                objRecorders[i].AddKeyFramesToAnimationCurve();
                MyUnityCurveContainer[] curves = objRecorders[i].curves;

                for (int x = 0; x < curves.Length; x++)
                {
                    _currAnimclip.SetCurve(objRecorders[i].pathName, typeof(Transform), curves[x].PropertyName,
                        curves[x].animCurve);
                }
            }

            /*if (recordBlendShape)
            {
                for (int i = 0; i < blendShapeRecorders.Count; i++)
                {

                    UnityCurveContainer[] curves = blendShapeRecorders[i].curves;

                    for (int x = 0; x < curves.Length; x++)
                    {
                        clip.SetCurve(blendShapeRecorders[i].pathName, typeof(SkinnedMeshRenderer), curves[x].propertyName, curves[x].animCurve);
                    }

                }
            }*/

            _currAnimclip.EnsureQuaternionContinuity();
            AssetDatabase.CreateAsset(_currAnimclip, exportFilePath);

            if (NewClipCreatedEvent != null)
                NewClipCreatedEvent.Invoke(_currAnimclip);

            CustomDebug(".anim file generated to " + exportFilePath);
           
        }

        void CustomDebug(string message)
        {
            if (showLogGUI)
                logMessage = message;
            else
                Debug.Log(message);
        }
    }
}
#endif