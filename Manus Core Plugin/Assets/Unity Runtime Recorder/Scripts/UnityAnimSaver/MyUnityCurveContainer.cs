﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Unity_Runtime_Recorder.Scripts.UnityAnimSaver
{
    public class MyUnityCurveContainer
    {
        public string PropertyName = "";
        public AnimationCurve animCurve;
        private List<Keyframe> _keyFrames;
        private int _maxNumberOfKeyFrames;
        private int _currentKeyFrameIndex = 0;

        public MyUnityCurveContainer(string propertyName, int maxNumberOfKeyFrames)
        {
            PropertyName = propertyName;
            _maxNumberOfKeyFrames = maxNumberOfKeyFrames;
            _keyFrames = new List<Keyframe>(_maxNumberOfKeyFrames);
        }

        public void AddKeyFrame(float animTime, float animValue)
        {
            if (_currentKeyFrameIndex < _maxNumberOfKeyFrames)
            {
                _keyFrames.Add(new Keyframe(animTime, animValue, 0.0f, 0.0f));
            }
        }

        public void AddKeyFramesToAnimationCurve()
        {
            animCurve = new AnimationCurve(_keyFrames.ToArray());
        }

        public void DeleteKeyFrame(int keyFrameIndex)
        {
            if (keyFrameIndex < _keyFrames.Count)
                _keyFrames.RemoveAt(keyFrameIndex);
        }

        public void ModifyKeyFrame(int keyFrameIndex, float animTime)
        {
            if (keyFrameIndex >= _keyFrames.Count)
                return;

            float animValue = _keyFrames[keyFrameIndex].value;
      
            _keyFrames[keyFrameIndex] = new Keyframe(animTime, animValue, 0.0f, 0.0f);
        }
    }
}