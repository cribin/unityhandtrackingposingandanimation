﻿using UnityEngine;

namespace Assets.Unity_Runtime_Recorder.Scripts.UnityAnimSaver
{
    public class MyUnityObjectAnimation
    {
        public MyUnityCurveContainer[] curves;
        public Transform observeGameObject;
        public string pathName;
        private int _numOfCurves = 7;

        public MyUnityObjectAnimation(string hierarchyPath, Transform observeObj, int maxNumberOfKeyFrames)
        {
            pathName = hierarchyPath;
            observeGameObject = observeObj;

            curves = new MyUnityCurveContainer[_numOfCurves];

            curves[0] = new MyUnityCurveContainer("localPosition.x", maxNumberOfKeyFrames);
            curves[1] = new MyUnityCurveContainer("localPosition.y", maxNumberOfKeyFrames);
            curves[2] = new MyUnityCurveContainer("localPosition.z", maxNumberOfKeyFrames);

            curves[3] = new MyUnityCurveContainer("localRotation.x", maxNumberOfKeyFrames);
            curves[4] = new MyUnityCurveContainer("localRotation.y", maxNumberOfKeyFrames);
            curves[5] = new MyUnityCurveContainer("localRotation.z", maxNumberOfKeyFrames);
            curves[6] = new MyUnityCurveContainer("localRotation.w", maxNumberOfKeyFrames);


            /* curves[7] = new MyUnityCurveContainer("localScale.x", maxNumberOfKeyFrames);
             curves[8] = new MyUnityCurveContainer("localScale.y", maxNumberOfKeyFrames);
             curves[9] = new MyUnityCurveContainer("localScale.z", maxNumberOfKeyFrames);*/
        }

        public void AddFrame(float time)
        {
            curves[0].AddKeyFrame(time, observeGameObject.localPosition.x);
            curves[1].AddKeyFrame(time, observeGameObject.localPosition.y);
            curves[2].AddKeyFrame(time, observeGameObject.localPosition.z);

            curves[3].AddKeyFrame(time, observeGameObject.localRotation.x);
            curves[4].AddKeyFrame(time, observeGameObject.localRotation.y);
            curves[5].AddKeyFrame(time, observeGameObject.localRotation.z);
            curves[6].AddKeyFrame(time, observeGameObject.localRotation.w);

            /*curves[7].AddKeyFrame(time, observeGameObject.localScale.x);
            curves[8].AddKeyFrame(time, observeGameObject.localScale.y);
            curves[9].AddKeyFrame(time, observeGameObject.localScale.z);*/
        }

        public void DeleteKeyFrame(int keyframeIndex)
        {
            for (int i = 0; i < _numOfCurves; i++)
                curves[i].DeleteKeyFrame(keyframeIndex);
        }

        public void AddKeyFramesToAnimationCurve()
        {
            foreach (MyUnityCurveContainer curve in curves)
            {
                curve.AddKeyFramesToAnimationCurve();
            }
        }

        public void ModifyKeyFrame(int keyFrameIndex, float keyFrameTimeStamp)
        {
            for (int i = 0; i < _numOfCurves; i++)
                curves[i].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp);
            /*curves[0].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localPosition.x);
            curves[1].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localPosition.y);
            curves[2].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localPosition.z);

            curves[3].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localRotation.x);
            curves[4].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localRotation.y);
            curves[5].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localRotation.z);
            curves[6].ModifyKeyFrame(keyFrameIndex, keyFrameTimeStamp, observeGameObject.localRotation.w);*/
            ;
        }
    }
}