﻿using UnityEngine;

public class ManusPoseEditMode : MonoBehaviour
{
    [SerializeField] private GameObject _physicalHandToModelHandMapper;
    [SerializeField] private GameObject _externalHandPosingController;

    [SerializeField] private KeyFrameAnimationViewer _keyFrameAnimationViewer;

    private HandednessSwitcher _physicalHandToModelHandednessSwitcher;
    private HandednessSwitcher _externalHandPosingHandednessSwitcher;

    private bool _externalPosingModeOn = true;

    // Use this for initialization
    void Start()
    {
        _physicalHandToModelHandMapper.SetActive(!_externalPosingModeOn);
        _externalHandPosingController.SetActive(_externalPosingModeOn);

        _physicalHandToModelHandednessSwitcher = _physicalHandToModelHandMapper.GetComponent<HandednessSwitcher>();
        _externalHandPosingHandednessSwitcher = _externalHandPosingController.GetComponent<HandednessSwitcher>();

        _keyFrameAnimationViewer.IsAnimationPlayingEvent += OnAnimationPlaybackChanged;
    }

    void OnDestroy()
    {
        _keyFrameAnimationViewer.IsAnimationPlayingEvent -= OnAnimationPlaybackChanged;
    }

    private void OnAnimationPlaybackChanged(bool isAnimationPlaying)
    {
        if (isAnimationPlaying)
        {
            if(_externalPosingModeOn)
                _externalHandPosingController.SetActive(false);
            else
                _physicalHandToModelHandMapper.SetActive(false);
        }
        else
        {
            if (_externalPosingModeOn)
                _externalHandPosingController.SetActive(true);
            else
                _physicalHandToModelHandMapper.SetActive(true);
        }
    }

    public void SwitchPosingMode()
    {
        _externalPosingModeOn = !_externalPosingModeOn;
        _physicalHandToModelHandMapper.SetActive(!_externalPosingModeOn);
        _externalHandPosingController.SetActive(_externalPosingModeOn);
    }

    public void SwitchHandedness()
    {
        _physicalHandToModelHandednessSwitcher.SwitchHandedness();
        _externalHandPosingHandednessSwitcher.SwitchHandedness();
    }
}