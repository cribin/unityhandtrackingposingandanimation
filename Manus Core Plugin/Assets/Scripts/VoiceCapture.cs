﻿using System;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class VoiceCapture : MonoBehaviour
{
    public event Action<PhraseRecognizedEventArgs> KeywordRecognizedEvent;
    [SerializeField] private string[] _keywords;
    [SerializeField] private ConfidenceLevel _confidenceLevel = ConfidenceLevel.Low;
    private KeywordRecognizer _recognizer;

    void Awake()
    {
        _recognizer = new KeywordRecognizer(_keywords, _confidenceLevel);
    }
    // Use this for initialization
    void OnEnable()
    {
        _recognizer.OnPhraseRecognized += OnPhraseRecognized;
        _recognizer.Start();
    }

    void OnDisable()
    {
        _recognizer.OnPhraseRecognized -= OnPhraseRecognized;
        _recognizer.Stop();
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        if (KeywordRecognizedEvent != null)
            KeywordRecognizedEvent.Invoke(args);
    }
}