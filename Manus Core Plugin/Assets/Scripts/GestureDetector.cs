﻿using System;
using System.Collections.Generic;
using Assets.ManusVR.Scripts;

using UnityEngine;

public enum Gesture
{
    IndexPoint,
    IndexMiddlePoint,
    PinkyPoint,
    ThumbBent,
    Grab,
    Pinch
}

public class GestureDetector : MonoBehaviour
{
    public HandData HandData;
    public bool IsLeftHanded = false;
    public device_type_t DeviceType { get; private set; }

    [Range(0f, 1f)] public float IndexGestureThreshold;
    [Range(0f, 1f)] public float IndexGestureOtherFingerThreshold;
    [Range(0f, 1f)] public float IndexMiddleGestureThreshold;
    [Range(0f, 1f)] public float PinkyPointGestureThreshold;
    [Range(0f, 1f)] public float ThumbBentGestureThreshold;
    [Range(0f, 1f)] public float GrabGestureThreshold;
    [Range(0f, 1f)] public float PinchGestureThreshold;
    private double[] _fingerSensorValues;
    private Vector3 _wristRotation;
   // private Quaternion _quaternionWrist = Quaternion.identity;
    private Dictionary<int, Func<bool>> _gestureToFunctionMapper = new Dictionary<int, Func<bool>>();

   // private Quaternion zeroQuat = new Quaternion(0.3f, 0.7f, 0.6f, -0.2f);

    void Start()
    {
        DeviceType = IsLeftHanded ? device_type_t.GLOVE_LEFT : device_type_t.GLOVE_RIGHT;
        _fingerSensorValues = new double[10];

        _gestureToFunctionMapper.Add((int) Gesture.IndexPoint, IsPointingWithIndexFinger);
        _gestureToFunctionMapper.Add((int) Gesture.IndexMiddlePoint, IsPointingWithIndexAndMiddleFinger);
        _gestureToFunctionMapper.Add((int) Gesture.PinkyPoint, IsPointingWithPinkyFinger);
        _gestureToFunctionMapper.Add((int)Gesture.ThumbBent, IsOnlyThumbBent);
        _gestureToFunctionMapper.Add((int) Gesture.Grab, IsGrabbing);
        _gestureToFunctionMapper.Add((int) Gesture.Pinch, IsPinching);
    }


    void Update()
    {
        _fingerSensorValues = HandData.GetFingerSensorValues(DeviceType);
        _wristRotation = HandData.CalibratedWristRotation(DeviceType).eulerAngles;
        //_quaternionWrist = HandData.CalibratedWristRotation(DeviceType);
       // IsPointingWithIndexFinger();
        //IsPinching();

        //Debug.Log("initial Wrist Angle:" + HandData.CalibratedWristRotation(DeviceType));
       // Debug.Log("Wrist Angle:" + Quaternion.Angle(HandData.CalibratedWristRotation(DeviceType), zeroQuat));
       //Debug.Log("Wristangles:" + _wristRotation);
    }

    public bool IsUsingGesture(Gesture gesture)
    {
        int gestureIndex = (int) gesture;
        if (_gestureToFunctionMapper.ContainsKey(gestureIndex))
            return _gestureToFunctionMapper[gestureIndex].Invoke();

        return false;
    }

    public bool IsHandFacingUp()
    {
        //close to 66-70

          if (_wristRotation.x >= 40f && _wristRotation.x <= 90f)
              return true;
        /*if (_quaternionWristW <= -0.2f && _quaternionWristW >= -0.4f)
            return true;*/
        //Debug.Log("not facing up " + _wristRotation.x);
        return false;
    }

    public bool IsHandFacingDown()
    {
        //close to 280
        if (_wristRotation.x >= 270f && _wristRotation.x <= 285f)
            return true;
        /*if (_quaternionWristW <= 0.8f && _quaternionWristW >= 0.6f)
            return true;*/

        return false;
    }

    public Vector3 GetWristRotation()
    {
        return _wristRotation;
    }

    public bool IsHandFacingLeft()
    {
        //close to 348-353
        if (_wristRotation.x >= 4f && _wristRotation.x <= 18f)
            return true;
       /* if (_quaternionWristW <= 0.4f && _quaternionWristW >= 0.2f)
            return true;*/

        return false;
    }

    public bool IsHandFacingRight()
    {
        //close to 348-353
        if (_wristRotation.x >= 345f && _wristRotation.x <= 360f)
            return true;
       /* if (_quaternionWristW <= -0.6f && _quaternionWristW >= -0.8f)
            return true;*/

        return false;
    }

    bool IsPointingWithIndexFinger()
    {
        double pinkyRingMiddleLowerSensorAverage = 0;
        double indexSensorAverage = (_fingerSensorValues[6] + _fingerSensorValues[7]) * 0.5;
        double pinkyWeight = 0.25f;
        double otherFingerWeight = (1f - pinkyWeight) / 2f;
        for (int i = 0; i < 6; i += 2)
        {
            var weight = i / 2 == 0 ? pinkyWeight : otherFingerWeight;
            pinkyRingMiddleLowerSensorAverage +=  weight *_fingerSensorValues[i];
        }
       
      
        if (pinkyRingMiddleLowerSensorAverage >= (IndexGestureOtherFingerThreshold) &&
            indexSensorAverage < IndexGestureThreshold)
        {
            return true;
        }

        return false;
    }

    bool IsPointingWithIndexAndMiddleFinger()
    {
        double pinkyRingLowerSensorAverage = 0;
        double indexMiddleSensorAverage = (_fingerSensorValues[4] + _fingerSensorValues[5] + _fingerSensorValues[6] +
                                           _fingerSensorValues[7]) * 0.25;

        for (int i = 0; i < 4; i += 2)
        {
            pinkyRingLowerSensorAverage += _fingerSensorValues[i];
        }
        pinkyRingLowerSensorAverage /= 2;
        if (pinkyRingLowerSensorAverage >= (1f - IndexMiddleGestureThreshold) &&
            indexMiddleSensorAverage < IndexMiddleGestureThreshold)
        {
            return true;
        }

        return false;
    }

    private bool IsPointingWithPinkyFinger()
    {
        double ringMiddleIndexLowerSensorAverage = 0;
        double pinkySensorAverage = (_fingerSensorValues[0] + _fingerSensorValues[1]) * 0.5;
        for (int i = 2; i < 8; i += 2)
        {
            ringMiddleIndexLowerSensorAverage += _fingerSensorValues[i];
        }
        ringMiddleIndexLowerSensorAverage /= 3;
        if (ringMiddleIndexLowerSensorAverage >= (1f - PinkyPointGestureThreshold) &&
            pinkySensorAverage < PinkyPointGestureThreshold)
        {
            return true;
        }

        return false;
    }

    private bool IsOnlyThumbBent()
    {
        double pinkyRingMiddleIndexLowerSensorAverage = 0;
        double thumbSensorAverage = _fingerSensorValues[9];
        for (int i = 0; i < 8; i += 2)
        {
            pinkyRingMiddleIndexLowerSensorAverage += _fingerSensorValues[i];
        }
        pinkyRingMiddleIndexLowerSensorAverage /= 4;
       // Debug.Log("thumb:" + thumbSensorAverage);
        //Debug.Log("other:" + pinkyRingMiddleIndexLowerSensorAverage);
        if (pinkyRingMiddleIndexLowerSensorAverage < (1f - ThumbBentGestureThreshold) &&
            thumbSensorAverage >= ThumbBentGestureThreshold)
        {
            return true;
        }

        return false;
    }


    bool IsGrabbing()
    {
        double totalFingerSensorAverage = 0;
        double pinkyWeight = 0.1f;
        double otherFingerWeight = (1f - pinkyWeight) / 4f;
        for (int i = 0; i < 10; i += 2)
        {
            var weight = i / 2 == 0 ? pinkyWeight : otherFingerWeight;

            totalFingerSensorAverage +=  weight * _fingerSensorValues[i];
        }
        //totalFingerSensorAverage *= 0.2f;
        //Debug.Log("grab: " + totalFingerSensorAverage);
        if (totalFingerSensorAverage >= GrabGestureThreshold)
            return true;

        return false;
    }

    bool IsPinching()
    {
        double indexThumbAverage = 0.7 * _fingerSensorValues[6] + 0.3 * _fingerSensorValues[8];

        if (indexThumbAverage >= PinchGestureThreshold)
            return true;

        return false;
    }
}