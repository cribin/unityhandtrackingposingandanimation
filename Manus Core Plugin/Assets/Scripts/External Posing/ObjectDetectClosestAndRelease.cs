﻿using Assets.ManusVR.Scripts;
using Assets.Scripts;
using UnityEngine;
using Valve.VR;

public class ObjectDetectClosestAndRelease : MonoBehaviour
{
    [SerializeField] private Gesture _detectionGesture = Gesture.Grab;
    public GestureDetector GestureDetector;
    public HandData HandData;

    private GameObject _heldGameObject;

    private bool _isHandGrabbing;

    private const float MaxDistance = 100;
    private float _smallestDistance = MaxDistance;
    private GameObject _closestGameObject;
    private GameObject _visualizedGameObject;

    // Use this for initialization
    void Start()
    {
    }

    void OnTriggerStay(Collider col)
    {
        if (_heldGameObject != null)
            return;

        float distance = Vector3.Distance(transform.position, col.gameObject.transform.position);
        if (distance < _smallestDistance)
        {
            _smallestDistance = distance;
            _closestGameObject = col.gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _isHandGrabbing = GestureDetector.IsUsingGesture(_detectionGesture);

        if (_heldGameObject == null && _smallestDistance < MaxDistance)
        {
            VisualizeClosestObject();
            CheckAndGrabObject(_closestGameObject);
        }

        CheckAndReleaseObject();

        _smallestDistance = MaxDistance;
    }

    void CheckAndGrabObject(GameObject objectToBeGrabbed)
    {
        if (!_isHandGrabbing || _heldGameObject != null)
            return;

        IHandInteractableObject handInteractableObject = objectToBeGrabbed.GetComponent<IHandInteractableObject>();
        if (handInteractableObject != null)
        {
            HandData.VibrateHand(GestureDetector.DeviceType, 0.7f, 300);

            handInteractableObject.Attach(gameObject);

            _heldGameObject = objectToBeGrabbed;
        }
    }


    void CheckAndReleaseObject()
    {
        if (_heldGameObject == null || _isHandGrabbing)
            return;

        IHandInteractableObject handInteractableObject = _heldGameObject.GetComponent<IHandInteractableObject>();
        handInteractableObject.Detach();

        _heldGameObject = null;
    }

    void VisualizeClosestObject()
    {
        if (_smallestDistance >= MaxDistance)
            return;

        if (_visualizedGameObject == _closestGameObject)
            return;

        SphereColliderVisualizer visualizer = _closestGameObject.GetComponent<SphereColliderVisualizer>();
        if (visualizer != null)
        {
            visualizer.SetVisualizerActive();
            _visualizedGameObject = _closestGameObject;
        }
    }

    public bool IsHandHoldingObject()
    {
        return _heldGameObject != null;
    }
}