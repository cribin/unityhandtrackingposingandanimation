﻿
using Assets.Scripts;
using UnityEngine;

public class BaseHandInteractableObject : MonoBehaviour, IHandInteractableObject
{
    private Transform _orignalParent;

    void Start()
    {
        _orignalParent = transform.parent;
    }

    public void Attach(GameObject parentObject)
    {
        transform.parent = parentObject.transform.parent;
    }

    public void Detach()
    {
        transform.parent = _orignalParent;
    }
}
