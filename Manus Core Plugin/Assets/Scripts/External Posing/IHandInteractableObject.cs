﻿using UnityEngine;

namespace Assets.Scripts
{
    public interface IHandInteractableObject
    {
        void Attach(GameObject parentObject);

        void Detach();
    }
}