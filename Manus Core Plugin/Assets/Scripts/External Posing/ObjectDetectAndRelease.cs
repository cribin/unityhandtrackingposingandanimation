﻿using Assets.ManusVR.Scripts;
using Assets.Scripts;
using UnityEngine;

public class ObjectDetectAndRelease : MonoBehaviour
{
    [SerializeField] private Gesture _detectionGesture;
    public GestureDetector GestureDetector;
    public HandData HandData;

    private GameObject _heldGameObject;

    private bool _isHandGrabbing;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        _isHandGrabbing = GestureDetector.IsUsingGesture(Gesture.Grab);


        CheckAndReleaseObject();
    }

    void OnTriggerStay(Collider col)
    {
        CheckAndGrabObject(col.gameObject);
    }

    void CheckAndGrabObject(GameObject objectToBeGrabbed)
    {
        if (!_isHandGrabbing || _heldGameObject != null)
            return;

        IHandInteractableObject handInteractableObject = objectToBeGrabbed.GetComponent<IHandInteractableObject>();
        if (handInteractableObject != null)
        {
            HandData.VibrateHand(GestureDetector.DeviceType, 0.7f, 300);

            handInteractableObject.Attach(gameObject);

            _heldGameObject = objectToBeGrabbed;
        }
    }


    void CheckAndReleaseObject()
    {
        if (_heldGameObject == null || _isHandGrabbing)
            return;

        IHandInteractableObject handInteractableObject = _heldGameObject.GetComponent<IHandInteractableObject>();
        handInteractableObject.Detach();

        _heldGameObject = null;
    }

    public bool IsHandHoldingObject()
    {
        return _heldGameObject != null;
    }
}