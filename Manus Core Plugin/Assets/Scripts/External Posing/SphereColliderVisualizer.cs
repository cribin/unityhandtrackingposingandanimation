﻿using System.Collections;
using UnityEngine;

public class SphereColliderVisualizer : MonoBehaviour
{
    private GameObject _visualizedSphereCollider;
    private readonly float _activeSeconds = 0.5f;

    public void Setup(Vector3 sphereColliderCenter, float sphereColliderRadius, Material sphereMaterial)
    {
        _visualizedSphereCollider = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        var sphereRenderer = _visualizedSphereCollider.GetComponent<Renderer>();
        sphereRenderer.material = sphereMaterial;
        _visualizedSphereCollider.transform.position = sphereColliderCenter;
        _visualizedSphereCollider.transform.localScale = Vector3.one * sphereColliderRadius;
        _visualizedSphereCollider.transform.parent = transform;
        _visualizedSphereCollider.SetActive(false);
    }

   /* void OnTriggerEnter()
    {
        _visualizedSphereCollider.SetActive(true);
        StartCoroutine(VisualizerDisabler());
    }*/

    public void SetVisualizerActive()
    {
        if (_visualizedSphereCollider.activeSelf)
            return;
        _visualizedSphereCollider.SetActive(true);
        StartCoroutine(VisualizerDisabler());
    }

    /*void OnTriggerExit()
    {
        _visualizedSphereCollider.SetActive(false);
    }*/

    IEnumerator VisualizerDisabler()
    {
        yield return new WaitForSeconds(_activeSeconds);
        _visualizedSphereCollider.SetActive(false);

    }
}