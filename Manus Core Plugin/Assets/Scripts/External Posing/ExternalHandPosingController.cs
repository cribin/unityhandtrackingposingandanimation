﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class ExternalHandPosingController : MonoBehaviour
    {
        //TODO : Encorportate other colliders for the joints of the model
        private enum ColliderType
        {
            SphereCollider,
            BoxCollider,
            CapsuleCollider
        }

        [SerializeField] private IKControl _ikControl;

        [SerializeField] private Transform _modelRootTransform;

        [SerializeField] ColliderType _colliderType;

        [SerializeField] private float _colliderRadius;

        [SerializeField] private GameObject _handsRootGameObject;

        [SerializeField] private Material _sphereColliderVisualizerMat;

        private List<SphereCollider> _colliderComponents =
            new List<SphereCollider>();

        private bool _fkOn = false;

        void Awake()
        {
            _fkOn = !_ikControl.IsIKActive();
            _ikControl.FKIKSwitchedEvent += OnFKIKSwitched;
            Transform[] allChildren = _modelRootTransform.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren.Skip(1))
            {
                var currGameObject = child.gameObject;
                AddTriggerColliderToGameObject(currGameObject);
                AddHandInteractableScriptToGameObject(currGameObject);
            }
        }

        void OnDestroy()
        {
            _ikControl.FKIKSwitchedEvent -= OnFKIKSwitched;
        }

        void Update()
        {
            _handsRootGameObject.SetActive(!CameraMode.Instance.IsCameraModeOn);
        }

        void AddTriggerColliderToGameObject(GameObject currGameObject)
        {
            var colliderComponent = currGameObject.AddComponent<SphereCollider>();
            colliderComponent.radius = _colliderRadius;
            colliderComponent.isTrigger = true;
            if (!_fkOn)
                colliderComponent.enabled = false;
            _colliderComponents.Add(colliderComponent);

            var sphereColliderVisualizer = currGameObject.AddComponent<SphereColliderVisualizer>();
            sphereColliderVisualizer.Setup(currGameObject.transform.position, colliderComponent.radius*3, _sphereColliderVisualizerMat);
        }

        void AddHandInteractableScriptToGameObject(GameObject currGameObject)
        {
            currGameObject.AddComponent<NonPhysicalHandInteractableObject>();
        }

        void ActivateColliderComponents()
        {
            foreach (var colliderComponent in _colliderComponents)
            {
                colliderComponent.enabled = true;
            }
        }

        void DeActivateColliderComponents()
        {
            foreach (var colliderComponent in _colliderComponents)
            {
                colliderComponent.enabled = false;
            }
        }

        public void OnFKIKSwitched()
        {
            _fkOn = !_fkOn;
            if (_fkOn)
                ActivateColliderComponents();
            else
                DeActivateColliderComponents();
        }
    }
}