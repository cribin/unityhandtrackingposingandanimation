﻿using UnityEngine;

public class AssignColliderToObjectAndChildren : MonoBehaviour
{
    private enum ColliderType
    {
        SphereCollider,
        BoxCollider,
        CapsuleCollider
    }

    [SerializeField] ColliderType _colliderType;

    [SerializeField] private float _colliderRadius;

    void Awake()
    {
        AddCollidersToObjectAndChildren();
    }


    void AddCollidersToObjectAndChildren()
    {
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            AddColliderToObject(child.gameObject);
        }
    }

    void AddColliderToObject(GameObject currGameObject)
    {
        var colliderComponent = currGameObject.AddComponent<SphereCollider>();
        colliderComponent.radius = _colliderRadius;
        colliderComponent.isTrigger = true;
    }
}