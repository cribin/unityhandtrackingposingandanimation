﻿using UnityEngine;

public class ObjectSelector : MonoBehaviour {

    public GestureDetector GestureDetector;

    private GameObject _selectedGameObject;

    public GameObject IndexTip;

    private bool _isIndexPointing;
    // Use this for initialization
    void Start () {
		
        if(IndexTip == null)
            Debug.Log("Index Tip is not specified in the ObjectSelector");
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _isIndexPointing = GestureDetector.IsUsingGesture(Gesture.IndexPoint);
        if(_isIndexPointing)
            RaycastFromIndexFinger();
	}

    void RaycastFromIndexFinger()
    {
        Ray handRay = new Ray(IndexTip.transform.position, IndexTip.transform.position - transform.position);
        RaycastHit rayCastHit;
        if (Physics.Raycast(handRay, out rayCastHit))
        {
            Debug.DrawLine(handRay.origin, rayCastHit.point);
        }
    }
}
