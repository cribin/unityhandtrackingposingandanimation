﻿using Assets.Scripts;
using UnityEngine;

public class NonPhysicalHandInteractableObject : MonoBehaviour, IHandInteractableObject
{
    private Transform _tempParent;
    private Quaternion _startParentRotation = Quaternion.identity;
    private Quaternion _startChildRotation;

    void Update()
    {
        if (_tempParent == null)
            return;

        transform.rotation = _tempParent.rotation * Quaternion.Inverse(_startParentRotation) * _startChildRotation;
    }

    public void Attach(GameObject parentObject)
    {
        if (_tempParent == null)
        {
            _tempParent = parentObject.transform;
            _startChildRotation = transform.rotation;
            _startParentRotation = _tempParent.rotation;
        }
    }

    public void Detach()
    {
        _tempParent = null;
        _startParentRotation = Quaternion.identity;
    }
}