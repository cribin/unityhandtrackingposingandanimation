﻿using Assets.Scripts;
using UnityEngine;

public class PhysicalHandInteractableObject : MonoBehaviour, IHandInteractableObject
{
    public bool KinematicWhenGrabbed;
    private Rigidbody _rigidbody;

    private Shader _highlightShader;
    private Shader _standardShader;
    private Renderer _rend;
    // Use this for initialization
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rend = GetComponent<Renderer>();
        _highlightShader = Shader.Find("Outlined/Silhouetted Diffuse");
        _standardShader = Shader.Find("Standard");
    }

    public void Attach(GameObject parentObject)
    {
        if (KinematicWhenGrabbed)
            _rigidbody.isKinematic = true;

        transform.parent = parentObject.transform;
        _rend.material.shader = _highlightShader;
    }

    public void Detach()
    {
        if (KinematicWhenGrabbed)
            _rigidbody.isKinematic = false;

        transform.parent = null;
        _rend.material.shader = _standardShader;
    }
}