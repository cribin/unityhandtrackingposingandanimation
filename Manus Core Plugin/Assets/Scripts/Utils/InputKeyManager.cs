﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum InputKey
{
    CaptureFrame,
    PlayPauseAnimation,
    ChangeHandedness,
    SwitchCameraMode,
    SwitchInternalExternalPosing,
    CameraReset,
    IkFkSwitch,
    SwitchUITransparency,
    None
}

public enum FingerModificationInputKey
{
    ModifyOnlyThumb,
    ModifyOnlyIndex,
    ModifyOnlyMiddle,
    ModifyOnlyRing,
    ModifyOnlyPinky
}

public enum InternalPosingModesInputKey
{
    ModifyOnlyHand,
    ModifyOnlyArm,
    ModifyArmAndHand
}

public enum CameraViewMode
{
    Front,
    Back,
    Left,
    Right
}

public class InputKeyManager : Singleton<InputKeyManager>
{
    [SerializeField] private KeyCode _captureFrameKey = KeyCode.C;

    [SerializeField] private KeyCode _playPauseAnimationKey = KeyCode.T;

    [SerializeField] private KeyCode _changeHandednessKey = KeyCode.H;

    [SerializeField] private KeyCode _switchCameraModeKey = KeyCode.G;

    [SerializeField] private KeyCode _externalInternalPosingKey = KeyCode.E;

    [SerializeField] private KeyCode _cameraResetKey = KeyCode.R;

    [SerializeField] private KeyCode _ikFkSwitchKey = KeyCode.F;

    [SerializeField] private KeyCode _switchUITransparency = KeyCode.P;

    [SerializeField] private KeyCode[] _modifyFingerKeys = new KeyCode[5];

    [SerializeField] private KeyCode[] _internalPosingModesKeys = new KeyCode[3];

    [SerializeField] private KeyCode[] _cameraViewModesKeys = new KeyCode[4];

    private KeyCode _noneKey = KeyCode.None;

    private readonly Dictionary<int, KeyCode> _inputKeyToKeyCodeMapper = new Dictionary<int, KeyCode>();
    private readonly Dictionary<int, KeyCode> _fingerModificationToKeyCodeMapper = new Dictionary<int, KeyCode>();
    private readonly Dictionary<int, KeyCode> _internalPosingModesToKeyCodeMapper = new Dictionary<int, KeyCode>();
    private readonly Dictionary<int, KeyCode> _cameraViewModesToKeyCodeMapper = new Dictionary<int, KeyCode>();

    void Start()
    {
        _inputKeyToKeyCodeMapper.Add((int) InputKey.CaptureFrame, _captureFrameKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.PlayPauseAnimation, _playPauseAnimationKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.ChangeHandedness, _changeHandednessKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.SwitchCameraMode, _switchCameraModeKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.SwitchInternalExternalPosing, _externalInternalPosingKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.CameraReset, _cameraResetKey);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.IkFkSwitch, _ikFkSwitchKey);
        _inputKeyToKeyCodeMapper.Add((int)InputKey.SwitchUITransparency, _switchUITransparency);
        _inputKeyToKeyCodeMapper.Add((int) InputKey.None, _noneKey);

        int index = 0;
        foreach (FingerModificationInputKey fingerModificationKey in Enum.GetValues(typeof(FingerModificationInputKey)))
        {
            if (index >= _modifyFingerKeys.Length)
                break;

            _fingerModificationToKeyCodeMapper.Add((int) fingerModificationKey, _modifyFingerKeys[index]);
            index++;
        }

        index = 0;
        foreach (InternalPosingModesInputKey internalPosingModeKey in Enum.GetValues(
            typeof(InternalPosingModesInputKey)))
        {
            if (index >= _internalPosingModesKeys.Length)
                break;

            _internalPosingModesToKeyCodeMapper.Add((int) internalPosingModeKey, _internalPosingModesKeys[index]);
            index++;
        }

        index = 0;
        foreach (CameraViewMode cameraViewMode in Enum.GetValues(
            typeof(CameraViewMode)))
        {
            if (index >= _cameraViewModesKeys.Length)
                break;

            _cameraViewModesToKeyCodeMapper.Add((int)cameraViewMode, _cameraViewModesKeys[index]);
            index++;
        }
    }

    public KeyCode GetKeyCode(InputKey inputKey)
    {
        int enumToInt = (int) inputKey;

        return _inputKeyToKeyCodeMapper[enumToInt];
    }

    public KeyCode GetKeyCode(FingerModificationInputKey inputKey)
    {
        int enumToInt = (int)inputKey;

        return _fingerModificationToKeyCodeMapper[enumToInt];
    }

    public KeyCode GetKeyCode(InternalPosingModesInputKey inputKey)
    {
        int enumToInt = (int)inputKey;

        return _internalPosingModesToKeyCodeMapper[enumToInt];
    }

    public KeyCode GetKeyCode(CameraViewMode inputKey)
    {
        int enumToInt = (int)inputKey;

        return _cameraViewModesToKeyCodeMapper[enumToInt];
    }
}