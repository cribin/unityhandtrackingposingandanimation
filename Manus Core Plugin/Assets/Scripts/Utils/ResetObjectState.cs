﻿using UnityEngine;

public class ResetObjectState : MonoBehaviour
{
    public KeyCode ResetPositionKey = KeyCode.R;

    private Vector3 _initialPosition;

    private Quaternion _initialRotation;

    private Rigidbody _rigidBody;

    // Use this for initialization
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _initialPosition = transform.position;
        _initialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(ResetPositionKey))
        {
            ResetState();
        }
    }

    void ResetState()
    {
        if (_rigidBody != null)
        {
            _rigidBody.velocity = Vector3.zero;
            _rigidBody.angularVelocity = Vector3.zero;
        }

        transform.position = _initialPosition;
        transform.rotation = _initialRotation;
    }
}
