﻿using UnityEngine;

enum HandUsed
{
    Left,
    Right,
    Both
}
public class HandednessSwitcher : MonoBehaviour
{
    [SerializeField] private GameObject _leftHandController;
    [SerializeField] private GameObject _rightHandController;

    private HandUsed _handUsed = HandUsed.Left;
    private readonly int _numOfOptions = 3;

    void Start()
    {
        if (_handUsed == HandUsed.Left)
        {
            _leftHandController.SetActive(true);
            _rightHandController.SetActive(false);
        }
    }

    public void SwitchHandedness()
    {
        if (_handUsed == HandUsed.Both)
            _handUsed = HandUsed.Left;
        else
            _handUsed = _handUsed + 1;

        if (_handUsed == HandUsed.Left)
        {
            _leftHandController.SetActive(true);
            _rightHandController.SetActive(false);
        }
        else if(_handUsed == HandUsed.Right)
        { 
            _rightHandController.SetActive(true);
            _leftHandController.SetActive(false);
        }
        else if (_handUsed == HandUsed.Both)
        {
            _rightHandController.SetActive(true);
            _leftHandController.SetActive(true);
        }

    }
}