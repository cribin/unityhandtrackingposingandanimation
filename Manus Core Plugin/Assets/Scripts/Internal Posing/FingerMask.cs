﻿using System;
using UnityEngine;

public enum FingerType
{
    Thumb,
    Index,
    Middle,
    Ring,
    Pinky,
}

public class FingerMask : MonoBehaviour
{
    private const int NumOfFingers = 5;

    private bool[] _fingerModificationDisabled = new bool[NumOfFingers];
    private bool _wristModificationDisabled = false;

    private int _maskedFinger = -1;

    void Update()
    {
        foreach (FingerModificationInputKey fingerModificationKey in Enum.GetValues(typeof(FingerModificationInputKey)))
        {
            if (Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(fingerModificationKey)))
            {
                ModifyOnlyGivenFinger((FingerType) fingerModificationKey);
            }
        }
    }

    void ModifyOnlyGivenFinger(FingerType fingerType)
    {
        if (_maskedFinger == -1)
        {
            _maskedFinger = (int) fingerType;
            DisableModificationForAllFingersExcept(fingerType);
        }
        else if (_maskedFinger == (int) fingerType)
        {
            _maskedFinger = -1;
            EnableModificationForAllFingers();
        }
        else
        {
            _maskedFinger = (int) fingerType;
            EnableModificationForAllFingers();
            DisableModificationForAllFingersExcept(fingerType);
        }
    }

    void EnableModificationForAllFingers()
    {
        _wristModificationDisabled = false;
        for (int i = 0; i < NumOfFingers; i++)
        {
            _fingerModificationDisabled[i] = false;
        }
    }

    void DisableModificationForAllFingersExcept(FingerType fingerType)
    {
        if(fingerType != FingerType.Thumb)
            _wristModificationDisabled = true;
        for (int i = 0; i < NumOfFingers; i++)
        {
            if ((FingerType) i == fingerType)
                continue;


            _fingerModificationDisabled[i] = true;
        }
    }

    public bool IsFingerDisabled(FingerType fingerType)
    {
        return _fingerModificationDisabled[(int) fingerType];
    }

    public bool IsWristDisabled()
    {
        return _wristModificationDisabled;
    }
}