﻿using System;
using Assets.Scripts;
using UnityEngine;

public class InternalHandPosingController : MonoBehaviour
{
    [SerializeField] private LeapIKHandlePositionSetter _leftIkHandlePositionSetter;
    [SerializeField] private LeapIKHandlePositionSetter _rightIkHandlePositionSetter;

    [SerializeField] private SynchronizeVirtualHandToPhysicalHand _leftVirtualHandController;
    [SerializeField] private SynchronizeVirtualHandToPhysicalHand _rightVirtualHandController;

    void Update()
    {
        foreach (InternalPosingModesInputKey internalPosingModesInputKey in Enum.GetValues(
            typeof(InternalPosingModesInputKey)))
        {
            if (Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(internalPosingModesInputKey)))
            {
                SwitchToCorrectInternalPosingMode(internalPosingModesInputKey);
            }
        }
    }

    private void SwitchToCorrectInternalPosingMode(InternalPosingModesInputKey internalPosingModesInputKey)
    {
        if (internalPosingModesInputKey == InternalPosingModesInputKey.ModifyOnlyHand)
        {
            _leftIkHandlePositionSetter.gameObject.SetActive(false);
            _rightIkHandlePositionSetter.gameObject.SetActive(false);
            _leftVirtualHandController.gameObject.SetActive(true);
            _rightVirtualHandController.gameObject.SetActive(true);
        }
        else if (internalPosingModesInputKey == InternalPosingModesInputKey.ModifyOnlyArm)
        {
            _leftIkHandlePositionSetter.gameObject.SetActive(true);
            _rightIkHandlePositionSetter.gameObject.SetActive(true);
            _leftVirtualHandController.gameObject.SetActive(false);
            _rightVirtualHandController.gameObject.SetActive(false);
        }
        else
        {
            _leftIkHandlePositionSetter.gameObject.SetActive(true);
            _rightIkHandlePositionSetter.gameObject.SetActive(true);
            _leftVirtualHandController.gameObject.SetActive(true);
            _rightVirtualHandController.gameObject.SetActive(true);
        }
    }
}