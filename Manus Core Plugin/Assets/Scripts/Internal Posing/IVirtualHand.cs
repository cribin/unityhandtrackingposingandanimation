﻿using UnityEngine;

namespace Assets.Scripts
{
    public interface IVirtualHand
    {
        void SetWristRotation(Quaternion wristRotation);
        void SetThumbJointRotations(Quaternion[] thumbJointRotatations);
        void SetFingerJointRotations(Quaternion[] fingerJointRotatations);
    }
}