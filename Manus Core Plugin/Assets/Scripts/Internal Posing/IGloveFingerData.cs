﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public interface IGloveFingerData
    {
        Quaternion GetWristRotation();
        Quaternion[] GetThumbJointRotations();
        Quaternion[] GetFingerJointRotations();
    }
}