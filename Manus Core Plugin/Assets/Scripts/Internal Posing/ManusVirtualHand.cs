﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class ManusVirtualHand : MonoBehaviour, IVirtualHand
    {
        [SerializeField] private FingerMask _fingerMask;
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private bool _isLeftHanded = true;
        [SerializeField] private Vector3 _posRotOffset = Vector3.zero;
        //[SerializeField] private Vector3 _posRotThumbOffset = Vector3.zero;
       // [SerializeField] private Vector3 _preRotOffset = Vector3.zero;
        //[SerializeField] private Transform _handRoot;
        [SerializeField] private Transform _wrist;
        [SerializeField] Transform[] _thumbJoints = new Transform[2];

        [SerializeField] Transform[] _fingerJoints = new Transform[12];

        //private readonly String[] _fingerNameSpecifiers = {"thumb", "index", "middle", "ring", "pinky"};
        private Quaternion _startWristRotation;

        private Quaternion[] _startFingerJointRotations = new Quaternion[12];
        private float _fingerBendDirection;
        private Quaternion _posRotOffsetQuaternion;
        private Quaternion _posRotOffsetThumbQuaternion;
        private Quaternion _preRotOffsetQuaternion;

        void Start()
        {
            _fingerBendDirection = _isLeftHanded ? -1f : 1f;
            CheckIfHandTransformsSet();
        }

        void CheckIfHandTransformsSet()
        {
            if (_wrist == null)
                Debug.LogError("Wrist-Transform not set. Please set it in the VirtualHand Script");
            else
                _startWristRotation = _wrist.rotation;

            if (_thumbJoints[0] == null || _thumbJoints[1] == null)
                Debug.LogError("Thumb-Joint-Transforms not set. Please set it in the VirtualHand Script");


            int currIndex = 0;
            foreach (Transform fingerJoint in _fingerJoints)
            {
                if (fingerJoint == null)
                {
                    Debug.LogError("Finger-Joint-Transforms not set. Please set it in the VirtualHand Script");
                    break;
                }
                _startFingerJointRotations[currIndex] = fingerJoint.localRotation;
                currIndex++;
            }
        }

        void SetFingerTransforms()
        {
            /*Transform[] allChildren = _wrist.GetComponentsInChildren<Transform>();
            Transform[] fingerJoints = new Transform[3];
            int fingerJointIndex = 0;
            //We skip the first transforms at it is the parent(wrist) itself
            //Search through all children transforms in the wrist throguh depth first search
            foreach (Transform child in allChildren.Skip(1))
            {
                _fingerNameSpecifiers.Contains(child.name)

            }*/
        }

        public void SetWristRotation(Quaternion wristRotation)
        {
            if (_fingerMask.IsWristDisabled())
                return;
           

            if (_isLeftHanded)
                _wrist.rotation = Quaternion.Euler(180f, _mainCamera.transform.eulerAngles.y, 0f) *
                                  wristRotation;
            else
            {
                _posRotOffsetQuaternion = Quaternion.Euler(_posRotOffset);
                _wrist.rotation = Quaternion.Euler(-180f, _mainCamera.transform.eulerAngles.y, 0f) *
                                  wristRotation * _posRotOffsetQuaternion;
            }

        }

        public void SetThumbJointRotations(Quaternion[] thumbJointRotatations)
        {
            if (_fingerMask.IsFingerDisabled(FingerType.Thumb))
                return;

            //_posRotOffsetThumbQuaternion = Quaternion.Euler(_posRotThumbOffset);

            /*
             * the 180 degrees of the first quaternion is required, since the physical left hand controls the virtual right hand
             * and vice versa. Camera rotation is required such that hand always moves correctly when camera is orbiting.
             */
            if (_isLeftHanded)
                _thumbJoints[0].rotation = Quaternion.Euler(180f, _mainCamera.transform.eulerAngles.y, 0f) *
                                           thumbJointRotatations[0]; // * _posRotOffsetThumbQuaternion;
            else
                _thumbJoints[0].rotation = Quaternion.Euler(-180f, _mainCamera.transform.eulerAngles.y, 0f) *
                                           thumbJointRotatations[0];// * _posRotOffsetThumbQuaternion;


            _thumbJoints[1].localRotation = thumbJointRotatations[1];
        }

        public void SetFingerJointRotations(Quaternion[] fingerJointRotatations)
        {
            for (int i = 0; i < _fingerJoints.Length; i++)
            {
                FingerType fingerType = (FingerType) ((i / 3) + 1);
                if (_fingerMask.IsFingerDisabled(fingerType))
                {
                    i += 2;
                    continue;
                }

                Quaternion fingerJointRotTemp = new Quaternion(fingerJointRotatations[i].x, fingerJointRotatations[i].z,
                    fingerJointRotatations[i].y, _fingerBendDirection * fingerJointRotatations[i].w);
                fingerJointRotTemp *= Quaternion.Inverse(fingerJointRotatations[i]);

                _fingerJoints[i].localRotation = fingerJointRotTemp * fingerJointRotatations[i];
            }
        }
    }
}