﻿using System;
using UnityEngine;


namespace Assets.Scripts
{
    public class SynchronizeVirtualHandToPhysicalHand : MonoBehaviour
    {
        private IVirtualHand _virtualHand;

        private IGloveFingerData _gloveFingerData;

        // Use this for initialization
        void Start()
        {
            _virtualHand = GetComponent<IVirtualHand>();
            if (_virtualHand == null)
                Debug.LogError("VirtualHand not set. Please add a VirtualHand class to the" + gameObject.name +
                               " object");

            _gloveFingerData = GetComponent<IGloveFingerData>();
            if (_gloveFingerData == null)
                Debug.LogError(
                    "GloveFingerData not set. Please add a GloveFingerData class to the" + gameObject.name + " object");
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (CameraMode.Instance.IsCameraModeOn)
                return;

            _virtualHand.SetWristRotation(_gloveFingerData.GetWristRotation());
            _virtualHand.SetThumbJointRotations(_gloveFingerData.GetThumbJointRotations());
            _virtualHand.SetFingerJointRotations(_gloveFingerData.GetFingerJointRotations());
        }
    }
}