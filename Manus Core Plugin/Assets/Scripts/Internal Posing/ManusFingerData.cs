﻿using Assets.ManusVR.Scripts;
using UnityEngine;

namespace Assets.Scripts
{
    public class ManusFingerData : MonoBehaviour, IGloveFingerData
    {
        [SerializeField] private HandData _handData;
        [SerializeField]
        private device_type_t _deviceType;

        private Quaternion[] _fingerRotations;
        private Quaternion[] _thumbRotations;
        private readonly int _numOfFingerJoints = 12;
        private readonly int _numOfThumbJoints = 2;
        private readonly int _jointsPerFinger = 3;

        private Quaternion _lastThumbRotation = Quaternion.identity;

        void Start()
        {
            _fingerRotations = new Quaternion[_numOfFingerJoints];
            _thumbRotations = new Quaternion[_numOfThumbJoints];
        }

        public Quaternion GetWristRotation()
        {
            return _handData.RawWristRotation(_deviceType);
        }

        public Quaternion[] GetThumbJointRotations()
        {
            var thumbRotation = _handData.ValidOutput(_deviceType)
                ? _handData.GetThumbRotation(_deviceType)
                : _lastThumbRotation;
            _lastThumbRotation = thumbRotation;

            _thumbRotations[0] = thumbRotation;
            _thumbRotations[1] = _handData.GetFingerRotation(FingerIndex.thumb, _deviceType, 3);
            
            return _thumbRotations;

        }

        public Quaternion[] GetFingerJointRotations()
        {
            for (int finger = 1; finger < 5; finger++)
            {
                for (int joint = 1; joint <= _jointsPerFinger; joint++)
                {
                    int index = (finger - 1) * _jointsPerFinger + (joint - 1);
                    _fingerRotations[index] = _handData.GetFingerRotation((FingerIndex) finger, _deviceType, joint);
                }
            }
            return _fingerRotations;
        }
    }
}