﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class PytorchTcpServer : MonoBehaviour
{
    #region private members 	

    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener _tcpListener;

    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread _tcpListenerThread;

    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient _connectedTcpClient;

    private bool _isListening = true;

    public event Action<string> HandDataReceivedEvent;

    #endregion

    // Use this for initialization
    void Awake()
    {
        // Start TcpServer background thread 		
        _tcpListenerThread = new Thread(ListenForIncomingRequests) {IsBackground = true};
        _tcpListenerThread.Start();
    }

    public void StopListening()
    {
        _isListening = false;
    }

    void OnDestroy()
    {
        _isListening = false;
        Debug.Log("Stop listening");
    }

    /// <summary> 	
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncomingRequests()
    {
        try
        {
            // Create listener on localhost port 8052. 			
            _tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8052);
            _tcpListener.Start();
            Debug.Log("Server is listening");

            while (_isListening)
            {
                using (_connectedTcpClient = _tcpListener.AcceptTcpClient())
                {
                    // Get a stream object for reading 					
                    using (NetworkStream stream = _connectedTcpClient.GetStream())
                    {
                        var streamReader = new StreamReader(stream);
                        // Read incomming stream into byte arrary. 						
                        while(_isListening && streamReader.Peek() > -1)
                        {
                            string clientMessage = streamReader.ReadLine();
                            Debug.Log("client message received");//" as: " + clientMessage);
                            if(HandDataReceivedEvent != null)
                                HandDataReceivedEvent.Invoke(clientMessage);
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            _tcpListener.Stop();
            Debug.Log("SocketException " + socketException);
        }
        _tcpListener.Stop();
    }

    /* /// <summary> 	
     /// Send message to client using socket connection. 	
     /// </summary> 	
     private void SendMessage()
     {
         if (_connectedTcpClient == null)
         {
             return;
         }
 
         try
         {
             // Get a stream object for writing. 			
             NetworkStream stream = _connectedTcpClient.GetStream();
             if (stream.CanWrite)
             {
                 string serverMessage = "This is a message from your server.";
                 // Convert string message to byte array.                 
                 byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
                 // Write byte array to socketConnection stream.               
                 stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                 Debug.Log("Server sent his message - should be received by client");
             }
         }
         catch (SocketException socketException)
         {
             Debug.Log("Socket exception: " + socketException);
         }
     }*/
}