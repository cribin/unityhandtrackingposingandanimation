﻿using System;
using UnityEngine;

public class PytorchNetworkDeepHandManager : MonoBehaviour
{


    [SerializeField] private PytorchTcpServer _pytorchTcpServer;

    public event Action HandDataUpdatedEvent;

    private ISharpPython _sharpPython;
    private readonly string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";
    string pythonScriptPath = "C:\\Users\\chalu\\OneDrive\\Documents\\PrashanthHandPoseEstimationSource\\handPoseEstimation-master\\src\\UnitTests\\tcp_python_to_unity.py";
    //string pythonScriptPath = "C:\\Users\\chalu\\PycharmProjects\\pyTorchHelloWorld\\random_float_printer.py";

    private IPythonScriptOutputParser _pythonScriptOutputParser;
    private readonly char _delimiter = ' ';
    private readonly int _numOfData = 17 * 4;

    private DeepHand _deepHand;
    private readonly int _numOfFingers = 5;
    private readonly int numOfJointsPerFinger = 3;
    private readonly int dofFingerJointPosition = 3;
    private readonly int dofFingerJointRotation = 4;

    private float[] _pyTorchFloatOutput;
    private float[] _pyTorchFingerOutput;

    void Start()
    {
        _pytorchTcpServer.HandDataReceivedEvent += OnHandDataReceived;

        Debug.Log("Starting Client");
        _sharpPython = new PytorchSharpPython(filePythonExePath);
        string standardError;
        _sharpPython.ExecutePythonScript(pythonScriptPath, out standardError);

        _pythonScriptOutputParser = new PytorchHandScriptOutputParser(_delimiter, _numOfData);

        _deepHand = new DeepHand(numOfJointsPerFinger, dofFingerJointPosition, dofFingerJointRotation);

        _pyTorchFloatOutput = new float[_numOfData];
        _pyTorchFingerOutput = new float[_numOfData - 8];
    }

    void OnDestroy()
    {
        _pytorchTcpServer.HandDataReceivedEvent -= OnHandDataReceived;
        //_pytorchTcpServer.StopListening();
        
    }

    private void OnHandDataReceived(string handData)
    {
        //Parse output
        _pyTorchFloatOutput = _pythonScriptOutputParser.ParsePythonStringOutput(handData);
        Array.Copy(_pyTorchFloatOutput, 8, _pyTorchFingerOutput, 0, _pyTorchFingerOutput.Length);

        //Set wrist rotation
        _deepHand.SetWristRotation(new Quaternion(_pyTorchFloatOutput[0], _pyTorchFloatOutput[1], _pyTorchFloatOutput[2], _pyTorchFloatOutput[3]));
        //set output to deep hands
        _deepHand.SetFingerJointsRotation(_pyTorchFingerOutput);

        if (HandDataUpdatedEvent != null)
            HandDataUpdatedEvent.Invoke();
    }

    public Quaternion[] GetDeepHandFingerRotations()
    {
        return _deepHand.GetAllFingerJointsRotations();
    }

    public Quaternion GetWristRotation()
    {
        return _deepHand.WristRotation;
    }
}
