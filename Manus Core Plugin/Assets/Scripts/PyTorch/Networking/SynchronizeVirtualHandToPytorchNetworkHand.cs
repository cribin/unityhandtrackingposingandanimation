﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class SynchronizeVirtualHandToPytorchNetworkHand : MonoBehaviour {

    private IVirtualHand _virtualHand;

    [SerializeField] private PytorchNetworkDeepHandManager _pytorchNetworkDeepHandManager;

    private bool _handDataUpdated;
    // Use this for initialization
    void Start()
    {
        _virtualHand = GetComponent<IVirtualHand>();
        if (_virtualHand == null)
            Debug.LogError("VirtualHand not set. Please add a VirtualHand class to the" + gameObject.name +
                           " object");

        _pytorchNetworkDeepHandManager.HandDataUpdatedEvent += OnHandDataUpdated;
    }

    void OnDestroy()
    {
        _pytorchNetworkDeepHandManager.HandDataUpdatedEvent += OnHandDataUpdated;
    }

    void LateUpdate()
    {
        if (!_handDataUpdated || CameraMode.Instance.IsCameraModeOn)
            return;

        _virtualHand.SetWristRotation(_pytorchNetworkDeepHandManager.GetWristRotation());
        _virtualHand.SetFingerJointRotations(_pytorchNetworkDeepHandManager.GetDeepHandFingerRotations());

        _handDataUpdated = false;
    }

    void OnHandDataUpdated()
    {
        _handDataUpdated = true;
    }
}
