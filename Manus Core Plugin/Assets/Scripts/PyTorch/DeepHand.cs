﻿using System.Collections.Generic;
using UnityEngine;

public class DeepHand
{
    private readonly int _numofJointsPerFinger;
    private readonly int _numOfFingers = 5;
    //degrees of freedom for fingerjointposition(3= x,y,z)
    private readonly int _dofFingerJointPosition;

    private readonly int _numOfPositionDataPerFinger;
    //in total there are 17 finger joint rotations (2:thumb, 3:i,m,r,p)
    private readonly int _numOfRotationDataPerFinger;
    //degrees of freedom for fingerjointrotation(3= euler, 4 = quaternion)
    private readonly int _dofFingerJointRotation ;
    private Dictionary<int, Quaternion[]> _fingerJointsRotation;
    private Dictionary<int, Vector3[]> _fingerJointsPosition;

    private Quaternion[] _allFingerJointsRotations;

    public Quaternion WristRotation { get; private set; }

    public DeepHand(int numofJointsPerFinger, int dofFingerJointPosition, int dofFingerJointRotation)
    {
        _numofJointsPerFinger = numofJointsPerFinger;
        _dofFingerJointPosition = dofFingerJointPosition;
        _dofFingerJointRotation = dofFingerJointRotation;
        _numOfPositionDataPerFinger = _numofJointsPerFinger * _dofFingerJointPosition;
        _numOfRotationDataPerFinger = _numofJointsPerFinger * _dofFingerJointRotation;
        _fingerJointsPosition = new Dictionary<int, Vector3[]>();
        _fingerJointsRotation = new Dictionary<int, Quaternion[]>();
        _allFingerJointsRotations = new Quaternion[_numOfFingers * _numofJointsPerFinger];

        InitFingerJointsDictionaries();
    }

    private void InitFingerJointsDictionaries()
    {

        foreach (var fingerIndex in System.Enum.GetValues(typeof(FingerType)))
        {
            _fingerJointsPosition.Add((int)fingerIndex, new Vector3[_numofJointsPerFinger]);
            _fingerJointsRotation.Add((int)fingerIndex, new Quaternion[_numofJointsPerFinger]);
        }
       
    }

   /* /// <summary>
    /// Assumptions: 
    ///     - Fingers are ordered from thumb to pinky
    ///     - Joints are ordered from knuckle to fingertip
    /// </summary>
    /// <param name="fingerJointsPosition"></param>
     public void SetFingerJointsPosition(float[] fingerJointsPosition)
     {
         if (fingerJointsPosition.Length != _numofJointsPerFinger * _dofFingerJointPosition * _numOfFingers)
         {
             Debug.LogError("Number of finger joint data does not match expected data");
         }
         
     }*/


    public void SetWristRotation(Quaternion wristRotation)
    {
        WristRotation = wristRotation;//new Quaternion(wristRotation[0], wristRotation[1], wristRotation[2] , wristRotation[3]);
    }

    public void SetFingerJointsRotation(float[] fingerJointsRotation)
    {
        if (fingerJointsRotation.Length != _numOfRotationDataPerFinger * _numOfFingers)
        {
            Debug.LogError("Number of quaternions in the hand " + _numOfRotationDataPerFinger + " does not match given input: " + fingerJointsRotation.Length);
            return;
        }

        for (int fingerIndex = 0; fingerIndex < _numOfFingers; fingerIndex++)
        {
            int offset = fingerIndex * _numOfRotationDataPerFinger;
            int allFingerOffset = fingerIndex * _numofJointsPerFinger;
            int allFingerCounter = 0;
            for (int i = 0; i < _numOfRotationDataPerFinger; i += _dofFingerJointRotation)
            {
                var jointIndex = i / _dofFingerJointRotation;
                int index = i + offset;
                int allFingerIndex = allFingerCounter + allFingerOffset;
                _allFingerJointsRotations[allFingerIndex] = new Quaternion(fingerJointsRotation[index], fingerJointsRotation[index + 1],
                    fingerJointsRotation[index + 2], fingerJointsRotation[index + 3]);
                _fingerJointsRotation[fingerIndex][jointIndex] =
                    _allFingerJointsRotations[allFingerIndex];

                allFingerCounter++;
            }
        }
    }

    public Vector3[] GetFingerJointsPosition(FingerType fingerType)
    {
        return _fingerJointsPosition[(int) fingerType];
    }

    public Quaternion[] GetFingerJointsRotation(FingerType fingerType)
    {
        return _fingerJointsRotation[(int)fingerType];
    }

    public Quaternion[] GetAllFingerJointsRotations()
    {
        return _allFingerJointsRotations;
    }

}
