﻿using Assets.Scripts;
using UnityEngine;

public class PytorchVirtualHand : MonoBehaviour, IVirtualHand
{
    [SerializeField] private Transform _wrist;

    [SerializeField] Transform[] _fingerJoints = new Transform[15];

    // Use this for initialization
    void Start()
    {
    }

    public void SetWristRotation(Quaternion wristRotation)
    {
        _wrist.localRotation = wristRotation;
    }

    public void SetThumbJointRotations(Quaternion[] thumbJointRotatations)
    {
        //throw new System.NotImplementedException();
    }

    public void SetFingerJointRotations(Quaternion[] fingerJointRotatations)
    {
        for (int i = 0; i < _fingerJoints.Length; i++)
        {
            /*FingerType fingerType = (FingerType)((i / 3) + 1);
            if (_fingerMask.IsFingerDisabled(fingerType))
            {
                i += 2;
                continue;
            }*/

            _fingerJoints[i].localRotation = fingerJointRotatations[i];
        }
    }
}