﻿using System;
using UnityEngine;
using UnityEngine.Profiling;

public class PytorchDeepHandManager : MonoBehaviour
{

    private ISharpPython _sharpPython;
    private readonly string filePythonExePath = "C:\\Users\\chalu\\Miniconda3\\python.exe";
    //string pythonScriptPath = "C:\\Users\\chalu\\OneDrive\\Documents\\PrashanthHandPoseEstimationSource\\handPoseEstimation-master\\src\\UnitTests\\Tests.py";
    string pythonScriptPath = "C:\\Users\\chalu\\PycharmProjects\\pyTorchHelloWorld\\random_float_printer.py";
    
        private IPythonScriptOutputParser _pythonScriptOutputParser;
    private readonly char _delimiter = ' ';
    private readonly int _numOfData = 17 * 4;

    private DeepHand _deepHand;
    private readonly int _numOfFingers = 5;
    private readonly int numOfJointsPerFinger = 3;
    private readonly int dofFingerJointPosition = 3;
    private readonly int dofFingerJointRotation = 4;

    private float[] _pyTorchFloatOutput;
    private float[] _pyTorchFingerOutput;
    // Use this for initialization
    void Start () {
		
        _sharpPython = new PytorchSharpPython(filePythonExePath);
        _pythonScriptOutputParser = new PytorchHandScriptOutputParser(_delimiter, _numOfData);

        _deepHand = new DeepHand(numOfJointsPerFinger, dofFingerJointPosition, dofFingerJointRotation);

        _pyTorchFloatOutput = new float[_numOfData];
        _pyTorchFingerOutput = new float[_numOfData - 8];
	}
	
	// Update is called once per frame
	void Update ()
	{
	   
        //Call script
        string standardError;
	    string pytorchOutput = _sharpPython.ExecutePythonScript(pythonScriptPath, out standardError);
	    

	   
        //Parse output
        _pyTorchFloatOutput = _pythonScriptOutputParser.ParsePythonStringOutput(pytorchOutput);
	    Array.Copy(_pyTorchFloatOutput, 8, _pyTorchFingerOutput, 0, _pyTorchFingerOutput.Length);
	    

	   
        //set output to deep hands
        _deepHand.SetFingerJointsRotation(_pyTorchFingerOutput);
	  

    }

    public Quaternion[] GetDeepHandFingerRotations()
    {
        return _deepHand.GetAllFingerJointsRotations();
    }

    public Quaternion GetWristRotation()
    {
        return _deepHand.WristRotation;
    }

   
}
