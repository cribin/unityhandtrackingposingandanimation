﻿
using System;
using System.Globalization;
using UnityEngine;

public class PytorchHandScriptOutputParser : IPythonScriptOutputParser
{

    private readonly char _delimiterChar;
    private readonly int _numOfData;

    public PytorchHandScriptOutputParser(char delimiterChar, int numOfData)
    {
        _delimiterChar = delimiterChar;
        _numOfData = numOfData;
    }

    public float[] ParsePythonStringOutput(string pythonOutput)
    {
       
        string[] separatedOutputs = pythonOutput.Split(new[] { _delimiterChar }, StringSplitOptions.RemoveEmptyEntries);

        if (separatedOutputs.Length != _numOfData)
        {
            Debug.LogError("The number of parsed output elements " + separatedOutputs.Length + " does not match given total size of data " + _numOfData);
            return null;
        }

        float[] floatOutputs = new float[_numOfData];

        for (int i = 0; i < _numOfData; i++)
        {
            floatOutputs[i] = (float)Double.Parse(separatedOutputs[i], NumberStyles.Float, CultureInfo.InvariantCulture);
        }

        return floatOutputs;
    }
}
