﻿using Assets.Scripts;
using UnityEngine;

public class SynchronizeVirtualHandToDeepHand : MonoBehaviour {

    private IVirtualHand _virtualHand;

   [SerializeField] private PytorchDeepHandManager _pytorchDeepHandManager;

    // Use this for initialization
    void Start()
    {
        _virtualHand = GetComponent<IVirtualHand>();
        if (_virtualHand == null)
            Debug.LogError("VirtualHand not set. Please add a VirtualHand class to the" + gameObject.name +
                           " object");

       
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (CameraMode.Instance.IsCameraModeOn)
            return;

        _virtualHand.SetWristRotation(_pytorchDeepHandManager.GetWristRotation());
        _virtualHand.SetFingerJointRotations(_pytorchDeepHandManager.GetDeepHandFingerRotations());
    }
}
