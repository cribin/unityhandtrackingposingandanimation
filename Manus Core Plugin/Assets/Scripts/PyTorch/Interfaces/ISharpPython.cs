﻿
using UnityEngine;

/// <summary>
/// All classes that want to create a communication from Csharp to Python need to implement
/// this interface
/// </summary>
public interface ISharpPython
{
    string ExecutePythonScript(string filePythonScript, out string standardError);
}
