﻿


public interface IPythonScriptOutputParser
{
    float[] ParsePythonStringOutput(string pythonOutput);
}