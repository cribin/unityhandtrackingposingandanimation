﻿
using UnityEngine;

public interface IFileParser
{

    float[] ParseHandDataFile(string filePath);
}
