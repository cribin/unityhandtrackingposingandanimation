﻿using System;
using System.Diagnostics;
using UnityEngine.Profiling;

public class PytorchSharpPython : ISharpPython
{
    private readonly string _filePythonExePath;
    private Process _pythonScriptExeProcess;

    public PytorchSharpPython(string exePythonPath)
    {
        _filePythonExePath = exePythonPath;

        InitializePythonScriptExeProcess();
    }

    public string ExecutePythonScript(string filePythonScript, out string standardError)
    {
        string outputText = string.Empty;
        standardError = string.Empty;
        try
        {
            _pythonScriptExeProcess.StartInfo.Arguments = filePythonScript;
            _pythonScriptExeProcess.Start();
           
           // outputText = _pythonScriptExeProcess.StandardOutput.ReadToEnd();
           // outputText = outputText.Replace(Environment.NewLine, string.Empty);
            
            // standardError = _pythonScriptExeProcess.StandardError.ReadToEnd();
            //_pythonScriptExeProcess.WaitForExit();
        }
        catch (Exception ex)
        {
            string exceptionMessage = ex.Message;
            UnityEngine.Debug.Log("Exception occured while calling python script: " + filePythonScript + "\n Exception: "
                      + exceptionMessage);
        }
        return outputText;
    }

    private void InitializePythonScriptExeProcess()
    {
        _pythonScriptExeProcess = new Process
        {
            StartInfo = new ProcessStartInfo(_filePythonExePath)
            {
                UseShellExecute = false,
                RedirectStandardOutput = false,
                RedirectStandardError = true,
                CreateNoWindow = true
            }
        };
    }
}