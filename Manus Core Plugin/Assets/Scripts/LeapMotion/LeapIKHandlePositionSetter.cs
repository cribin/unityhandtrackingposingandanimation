﻿using Leap;
using Leap.Unity;
using UnityEngine;

public class LeapIKHandlePositionSetter : MonoBehaviour
{
    [SerializeField] private LeapProvider _leapProvider;

    [SerializeField] private GameObject _ikHandle;

    [SerializeField] private bool _isLeftHanded;

    [SerializeField] private Vector3 _positionScale = Vector3.one;

    [SerializeField] private Vector3 _positionOffset = Vector3.zero;


    void Start()
    {
        _leapProvider.OnUpdateFrame += OnUpdateFrame;
    }

    void OnDestroy()
    {
        _leapProvider.OnFixedFrame -= OnUpdateFrame;
    }

    private void OnUpdateFrame(Frame frame)
    {
        if (frame != null)
            UpdateIKHandlePositions(frame);
    }

    private void UpdateIKHandlePositions(Frame frame)
    {
        for (int i = 0; i < frame.Hands.Count; i++)
        {
            var curHand = frame.Hands[i];

            Vector3 handPosition = curHand.PalmPosition.ToVector3();
            if (curHand.IsLeft && _isLeftHanded)       
                SetWristPosition(_ikHandle, handPosition);
            else if (curHand.IsRight && !_isLeftHanded)       
                SetWristPosition(_ikHandle, handPosition);
            
        }
    }

    void SetWristPosition(GameObject hand, Vector3 position)
    {
        hand.transform.position = ScaleAndOffsetPosition(position);
        //hand.transform.position = -hand.transform.position[0];
    }

    Vector3 ScaleAndOffsetPosition(Vector3 leapPos)
    {
        return Vector3.Scale(leapPos, _positionScale) + _positionOffset;
    }
}