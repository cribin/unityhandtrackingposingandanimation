﻿using Leap;
using Leap.Unity;
using UnityEngine;

[RequireComponent(typeof(LeapProvider))]
public class LeapHandPositionSetter : MonoBehaviour
{
    [SerializeField] private MultiViewCameraController _multiViewCameraController;
    [SerializeField] private LeapProvider _leapProvider;
    [SerializeField] private GameObject _leftHand;

    [SerializeField] private GameObject _rightHand;

    [SerializeField] private ObjectDetectAndRelease _leftDetection;

    [SerializeField] private ObjectDetectAndRelease _rightDetection;

    [SerializeField] private IKControl _ikControl;

    [SerializeField] private Vector3 _positionScale = Vector3.one;

    [SerializeField] private Vector3 _positionOffset = Vector3.zero;

    private bool _fkOn = false;

    private Camera _activeCamera;

    //[SerializeField] private Vector3 _customRangeMin = Vector3.one * -1f;
    // [SerializeField] private Vector3 _customRangeMax = Vector3.one;
  
    // Use this for initialization
    void Start()
    {
        _leapProvider.OnUpdateFrame += OnUpdateFrame;

        _fkOn = !_ikControl.IsIKActive();
        _ikControl.FKIKSwitchedEvent += OnFKIKSwitched;
        _multiViewCameraController.ActiveCameraChangedEvent += OnActiveCameraChanged;
        _activeCamera = _multiViewCameraController.ActiveCamera;
    }

    void OnDestroy()
    {
        _leapProvider.OnFixedFrame -= OnUpdateFrame;
        _ikControl.FKIKSwitchedEvent -= OnFKIKSwitched;
        _multiViewCameraController.ActiveCameraChangedEvent -= OnActiveCameraChanged;
    }

    private void OnUpdateFrame(Frame frame)
    {
        if (frame != null)
            UpdateHandPositions(frame);
    }

    private void UpdateHandPositions(Frame frame)
    {
        for (int i = 0; i < frame.Hands.Count; i++)
        {
            var curHand = frame.Hands[i];
            Vector3 handPosition = curHand.PalmPosition.ToVector3();
            if (curHand.IsLeft)
            {
                if (!_fkOn || (_fkOn && !_leftDetection.IsHandHoldingObject()))
                    SetWristPosition(_leftHand, handPosition);
            }
            else if (curHand.IsRight)
            {
                if(!_fkOn || (_fkOn && !_rightDetection.IsHandHoldingObject()))
                    SetWristPosition(_rightHand, handPosition);
            }
        }
    }

    void SetWristPosition(GameObject hand, Vector3 position)
    {
        hand.transform.position = ScaleAndOffsetPosition(LeapSpaceToCameraSpace(position));
    }

    void SetWristRotation(GameObject hand, Quaternion rotation)
    {
        hand.transform.rotation = rotation;
    }

    Vector3 ScaleAndOffsetPosition(Vector3 leapPos)
    {
        return Vector3.Scale(leapPos, _positionScale) + _positionOffset;
    }

    Vector3 LeapSpaceToCameraSpace(Vector3 leapPosition)
    {
        return Quaternion.Euler(0f, _activeCamera.transform.eulerAngles.y, 0f) * leapPosition;
    }

    void OnFKIKSwitched()
    {
        _fkOn = !_fkOn;      
    }

    private void OnActiveCameraChanged(Camera activeCamera)
    {
        _activeCamera = activeCamera;
    }
}