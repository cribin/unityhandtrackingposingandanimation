﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[ExecuteInEditMode]
public class IKControl : MonoBehaviour
{
    protected Animator MyAnimator;

    [SerializeField] private bool _rotateIKTowardsTarget = false;
    [SerializeField] bool _ikActive = true;
    [SerializeField] Transform _leftHandTarget = null;
    [SerializeField] Transform _rightHandTarget = null;
    [SerializeField] Transform _leftFootTarget = null;
    [SerializeField] Transform _rightFootTarget = null;
    [SerializeField] Transform _lookObj = null;

    [SerializeField] float _leftHandTargetWeight = 1f;
    [SerializeField] float _rightHandTargetWeight = 1f;


    private AvatarIKGoal _leftHandGoal = AvatarIKGoal.LeftHand;
    private AvatarIKGoal _rightHandGoal = AvatarIKGoal.RightHand;
    private AvatarIKGoal _leftFootGoal = AvatarIKGoal.LeftFoot;
    private AvatarIKGoal _rightFootGoal = AvatarIKGoal.RightFoot;

    private Avatar _orignalAvatar;

    private float _resetIKWeight = 0f;

    private IKPosToFKPosMapper _ikPosToFkPosMapper;

    public event Action FKIKSwitchedEvent;

    void Start()
    {
        MyAnimator = GetComponent<Animator>();
        _orignalAvatar = MyAnimator.avatar;

        _ikPosToFkPosMapper = GetComponent<IKPosToFKPosMapper>();
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (!MyAnimator) return;
        //if the IK is active, set the position and rotation directly to the goal. 
        if (_ikActive)
        {
            // Set the look target position, if one has been assigned
            if (_lookObj != null)
            {
                MyAnimator.SetLookAtWeight(1);
                MyAnimator.SetLookAtPosition(_lookObj.position);
            }

            SetIKGoalToTarget(_leftHandGoal, _leftHandTarget, _leftHandTargetWeight);
            SetIKGoalToTarget(_rightHandGoal, _rightHandTarget, _rightHandTargetWeight);
            SetIKGoalToTarget(_leftFootGoal, _leftFootTarget, 1f);
            SetIKGoalToTarget(_rightFootGoal, _rightFootTarget, 1f);
        }

        //if the IK is not active, set the position and rotation of the hand and head back to the original position
        else
        {
            ResetIKGoal(_leftHandGoal, _resetIKWeight);
            ResetIKGoal(_rightHandGoal, _resetIKWeight);
            ResetIKGoal(_leftFootGoal, _resetIKWeight);
            ResetIKGoal(_rightFootGoal, _resetIKWeight);
            MyAnimator.SetLookAtWeight(0);
        }
    }

    void SetIKGoalToTarget(AvatarIKGoal currIKGoal, Transform currIKTarget, float ikWeight)
    {
        if (currIKTarget == null)
            return;

        MyAnimator.SetIKPositionWeight(currIKGoal, ikWeight);
        if (_rotateIKTowardsTarget)
            MyAnimator.SetIKRotationWeight(currIKGoal, ikWeight);
        MyAnimator.SetIKPosition(currIKGoal, currIKTarget.position);
        MyAnimator.SetIKRotation(currIKGoal, currIKTarget.rotation);
    }

    void ResetIKGoal(AvatarIKGoal currIKGoal, float ikWeight)
    {
        MyAnimator.SetIKPositionWeight(currIKGoal, ikWeight);
        MyAnimator.SetIKRotationWeight(currIKGoal, ikWeight);
    }

    void DisableAvatar()
    {
        _ikPosToFkPosMapper.CopyIKQuaternions();
        MyAnimator.avatar = null;
        _ikPosToFkPosMapper.ApplyIKQuaternions();
    }

    void EnableAvatar()
    {
        MyAnimator.avatar = _orignalAvatar;
    }

    public void OnFKIKSwitched()
    {
        _ikActive = !_ikActive;
        if (FKIKSwitchedEvent != null)
            FKIKSwitchedEvent.Invoke();
        if (_ikActive)
            EnableAvatar();
        else
            DisableAvatar();
    }

    public bool IsIKActive()
    {
        return _ikActive;
    }
}