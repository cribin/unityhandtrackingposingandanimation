﻿using UnityEngine;

public class IKTargetsManager : MonoBehaviour
{
    [SerializeField] private IKControl _ikControl;
    [SerializeField] private GameObject _ikHandlesParent;
    private readonly int _numOfIkTargets = 4;

    private GameObject[] _ikTargets;
    private Renderer[] _ikTargetsRenderer;

    private bool _fkActive;

    private bool _internalPosingActive = false;
    // Use this for initialization
    void Start()
    {
        _ikTargets = new GameObject[_numOfIkTargets];
        _ikTargetsRenderer = new Renderer[_numOfIkTargets];
        int index = 0;
        foreach (Transform child in _ikHandlesParent.transform)
        {
            if (index > _numOfIkTargets)
            {
                Debug.LogError("Warning: " + gameObject.name +
                               " must have exactly 4 IK Targets(left hand, right hand, left foot, right foot)");
                break;
            }
            _ikTargets[index] = child.gameObject;
            _ikTargetsRenderer[index] = child.GetComponent<Renderer>();
            index++;
        }

        _fkActive = !_ikControl.IsIKActive();
        _ikControl.FKIKSwitchedEvent += OnFKIKSwitched;
        if (_fkActive)
            DeActivateIKTargets();
    }

    void OnDestroy()
    {
        _ikControl.FKIKSwitchedEvent -= OnFKIKSwitched;
    }

    void ActivateIKTargets()
    {
        foreach (var ikTarget in _ikTargets)
            ikTarget.SetActive(true);
    }

    void DeActivateIKTargets()
    {
        foreach (var ikTarget in _ikTargets)
            ikTarget.SetActive(false);
    }

    void ActivateIKTargetsRenderer()
    {
        foreach (var ikTargetrenderer in _ikTargetsRenderer)
            ikTargetrenderer.enabled = true;
    }

    void DeActivateIKTargetsRenderer()
    {
        foreach (var ikTargetrenderer in _ikTargetsRenderer)
            ikTargetrenderer.enabled = false;
    }

    public void OnFKIKSwitched()
    {
        _fkActive = !_fkActive;
        if (_fkActive)
            DeActivateIKTargets();
        else
            ActivateIKTargets();
    }

    public void OnExternalInternalPosingSwitched()
    {
        _internalPosingActive = !_internalPosingActive;
        if(_internalPosingActive)
            DeActivateIKTargetsRenderer();
        else 
            ActivateIKTargetsRenderer();
    }
}