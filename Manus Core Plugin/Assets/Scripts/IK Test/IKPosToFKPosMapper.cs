﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

struct CopyQuaternion
{
    private float x, y, z, w;

    public float X
    {
        get { return x; }
    }

    public float Y
    {
        get { return y; }
    }

    public float Z
    {
        get { return z; }
    }

    public float W
    {
        get { return w; }
    }

    public CopyQuaternion(Quaternion quat)
    {
        x = quat.x;
        y = quat.y;
        z = quat.z;
        w = quat.w;
    }
}

public class IKPosToFKPosMapper : MonoBehaviour
{
    [SerializeField] private Transform _modelRootTransform;

    private List<Transform> _allModelTransforms = new List<Transform>();

    private List<CopyQuaternion> _test;

    private Vector3 _rootIKPosition;
    // Use this for initialization
    void Start()
    {
        Transform[] allChildren = _modelRootTransform.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            _allModelTransforms.Add(child);
        }
    }

    // Update is called once per frame
   /* public void DisableAvatar()
    {
        List<CopyQuaternion> _test = new List<CopyQuaternion>(_allModelTransforms.Count);
        foreach (Transform child in _allModelTransforms)
        {
            _test.Add(new CopyQuaternion(child.rotation));
        }


        int index = 0;
        foreach (Transform child in _allModelTransforms)
        {
            CopyQuaternion copyQuat = _test[index];
            child.rotation = new Quaternion(copyQuat.X, copyQuat.Y, copyQuat.Z, copyQuat.W);
            index++;
        }
    }*/

    public void CopyIKQuaternions()
    {
        _rootIKPosition = _modelRootTransform.position;
       _test = new List<CopyQuaternion>(_allModelTransforms.Count);
        foreach (Transform child in _allModelTransforms)
        {
            _test.Add(new CopyQuaternion(child.rotation));
        }
    }

    public void ApplyIKQuaternions()
    {

        if (_test.Count == 0)
            return;

        _modelRootTransform.position = _rootIKPosition;
        int index = 0;
        foreach (Transform child in _allModelTransforms)
        {
            CopyQuaternion copyQuat = _test[index];
            child.rotation = new Quaternion(copyQuat.X, copyQuat.Y, copyQuat.Z, copyQuat.W);
            index++;
        }

        _test.Clear();
    }
}