﻿using System.Collections;
using UnityEngine;

public class ScreenShotHandler : Singleton<ScreenShotHandler>
{
    private MultiViewCameraController _multiViewCameraController;
    private bool _takeScreenshotOnNextFrame;

    void Awake()
    {
        _multiViewCameraController = GetComponent<MultiViewCameraController>();
    }

    public void TakeFullScreenShot(int resWidth, int resHeight, KeyFrameTimeStamp keyFrameTimeStamp)
    {
        StartCoroutine(TakeScreenShot(resWidth, resHeight, keyFrameTimeStamp));
    }

    private IEnumerator TakeScreenShot(int resWidth, int resHeight, KeyFrameTimeStamp keyFrameTimeStamp)
    {
        yield return new WaitForEndOfFrame();

        Camera activeCamera = _multiViewCameraController.ActiveCamera;

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        activeCamera.targetTexture = rt;
        var currentScreenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        activeCamera.Render();
        RenderTexture.active = rt;
        currentScreenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        currentScreenShot.Apply();
        activeCamera.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);

        keyFrameTimeStamp.KeyFrameScreenshot = currentScreenShot;
    }
}