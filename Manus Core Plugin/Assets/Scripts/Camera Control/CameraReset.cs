﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraReset : MonoBehaviour
{
    private Vector3 _originalPosition;

    private Quaternion _originalRotation;

    // Use this for initialization
    void Start()
    {
        _originalPosition = transform.position;
        _originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!CameraMode.Instance.IsCameraModeOn)
            return;

        if (Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(InputKey.CameraReset)))
            ResetCamera();
    }

    void ResetCamera()
    {
        transform.position = _originalPosition;
        transform.rotation = _originalRotation;
    }
}