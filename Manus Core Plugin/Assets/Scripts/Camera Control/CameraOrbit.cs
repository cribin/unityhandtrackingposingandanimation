﻿using UnityEngine;


public class CameraOrbit : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _orbitSpeed;


    // Update is called once per frame
    void LateUpdate()
    {
        if (!CameraMode.Instance.IsCameraModeOn)
            return;

        Vector3 rotationAxis;
        float mouseMovement;
        bool leftMousePressed = Input.GetMouseButton(0);
        bool rightMousePressed = Input.GetMouseButton(1);

        if (leftMousePressed && !rightMousePressed)
        {
            rotationAxis = Vector3.up;
            mouseMovement = Input.GetAxis("Mouse X");
        }
        else if (!leftMousePressed && rightMousePressed)
        {
            rotationAxis = Vector3.right;
            mouseMovement = Input.GetAxis("Mouse Y");
        }
        else
            return;


        transform.RotateAround(_target.position, rotationAxis, mouseMovement * _orbitSpeed * Time.deltaTime);
    }
}