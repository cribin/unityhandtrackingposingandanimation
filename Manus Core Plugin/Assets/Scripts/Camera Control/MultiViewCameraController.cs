﻿using System;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class MultiViewCameraController : MonoBehaviour
{
    public event Action<Camera> ActiveCameraChangedEvent;
    public Camera ActiveCamera { get; private set; }

    [SerializeField] private Camera[] _cameras = new Camera[4];
    [SerializeField] private VoiceCapture _voiceCapture;

    private bool _voiceCaptureOn;
    private readonly int _numOfCameras = Enum.GetNames(typeof(CameraViewMode)).Length;
 
    void Awake()
    {
        ActiveCamera = _cameras[0];
        if (_cameras.Length > _numOfCameras)
        {
            Debug.LogError("The MultiViewCameraController can only have " + _numOfCameras + " cameras");
        }
    }

    void Start()
    {
        _voiceCaptureOn = _voiceCapture != null;
        if (_voiceCapture != null)    
            _voiceCapture.KeywordRecognizedEvent += OnKeyWordRecognized;
        
    }

    void OnDestroy()
    {
        if (_voiceCaptureOn)
            _voiceCapture.KeywordRecognizedEvent -= OnKeyWordRecognized;
    }

    void Update()
    {
        foreach (CameraViewMode cameraViewMode in Enum.GetValues(
            typeof(CameraViewMode)))
        {
            if (Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(cameraViewMode)))
            {
                ActivateGivenCamera(cameraViewMode);
            }
        }
    }

    void ActivateGivenCamera(CameraViewMode cameraViewMode)
    {
        for (int i = 0; i < _cameras.Length; i++)
        {
            if (i == (int) cameraViewMode)
            {
                _cameras[i].gameObject.SetActive(true);
                if (ActiveCameraChangedEvent != null)
                    ActiveCameraChangedEvent.Invoke(_cameras[i]);
                continue;
            }

            _cameras[i].gameObject.SetActive(false);
        }
    }

    private void OnKeyWordRecognized(PhraseRecognizedEventArgs keyword)
    {
        if(!CameraMode.Instance.IsCameraModeOn)
            return;

        foreach (CameraViewMode cameraViewMode in Enum.GetValues(
            typeof(CameraViewMode)))
        {
            if (keyword.text.Equals(cameraViewMode.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                ActivateGivenCamera(cameraViewMode);
            }
        }
    }
}