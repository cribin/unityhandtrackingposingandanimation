﻿using System;
using UnityEngine;

public class CameraMode : Singleton<CameraMode>
{
    public bool IsCameraModeOn { get; private set; }

    public event Action<bool> CameraModeSwitchedEvent;

    void Start()
    {
        IsCameraModeOn = false;
    }

    public void SwitchCameraMode()
    {
        IsCameraModeOn = !IsCameraModeOn;
        if (CameraModeSwitchedEvent != null)
            CameraModeSwitchedEvent.Invoke(IsCameraModeOn);
    }
}