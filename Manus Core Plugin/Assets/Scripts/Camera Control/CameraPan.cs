﻿using UnityEngine;


public class CameraPan : MonoBehaviour
{
    [SerializeField] private Vector2 _panSpeed = new Vector2(10, 10);
    [SerializeField] private bool _invertXPanDirection = false;
    [SerializeField] private bool _invertYPanDirection = false;

    // Update is called once per frame
    void LateUpdate()
    {
        if (!CameraMode.Instance.IsCameraModeOn || !Input.GetMouseButton(2))
            return;

        Vector3 translation = Vector3.zero;

        Vector2 invertPanDirection = new Vector2(_invertXPanDirection ? -1f : 1f,
            _invertYPanDirection ? -1f : 1f);

        float mouseX = Input.GetAxis("Mouse X");
        if (Mathf.Abs(mouseX) < 0.1)
            mouseX = 0f;
        float mouseY = Input.GetAxis("Mouse Y");
        if (Mathf.Abs(mouseY) < 0.1)
            mouseY = 0f;

        translation -= Quaternion.Inverse(transform.localRotation) * transform.right * mouseX * _panSpeed.x *
                       invertPanDirection.x * Time.deltaTime;

        translation -= Quaternion.Inverse(transform.localRotation) * transform.up * mouseY * _panSpeed.y *
                       invertPanDirection.y * Time.deltaTime;

        transform.Translate(translation);
    }
}