﻿using UnityEngine;

public class HandBasedCameraPan : MonoBehaviour
{

    [SerializeField] private GestureDetector _gestureDetector;
    [SerializeField] private Vector2 _panSpeed = new Vector2(10, 10);
    [SerializeField] private bool _invertXPanDirection = false;
    [SerializeField] private bool _invertYPanDirection = false;

    // Update is called once per frame
    void LateUpdate()
    {
        if (!CameraMode.Instance.IsCameraModeOn || !_gestureDetector.IsUsingGesture(Gesture.ThumbBent))
            return;

        Debug.Log("wrist:" + _gestureDetector.GetWristRotation());
        Vector3 translation = Vector3.zero;

        Vector2 invertPanDirection = new Vector2(_invertXPanDirection ? -1f : 1f,
            _invertYPanDirection ? -1f : 1f);

        float handX = 0f, handY = 0f;
        if (_gestureDetector.IsHandFacingDown())
            handY = 1f;
        else if (_gestureDetector.IsHandFacingUp())
            handY = -1f;

        /*if (_gestureDetector.IsHandFacingLeft())
            handX = 1f;
        else if (_gestureDetector.IsHandFacingRight())
            handX = -1f;*/

        translation -= Quaternion.Inverse(transform.localRotation) * transform.right * handX * _panSpeed.x *
                       invertPanDirection.x * Time.deltaTime;

        translation -= Quaternion.Inverse(transform.localRotation) * transform.up * handY * _panSpeed.y *
                       invertPanDirection.y * Time.deltaTime;

        transform.Translate(translation);
    }
}
