﻿using Leap;
using Leap.Unity;
using UnityEngine;

public class HandBasedCameraOrbit : MonoBehaviour
{
    [SerializeField] private GestureDetector _gestureDetector;
    [SerializeField] private Transform _target;
    [SerializeField] private float _orbitSpeed;

    private float _thresholdedPalmVelocity ;
    private bool _isGrabbing;
    private float _isHandFacingUp = 1f;

    // Update is called once per frame
    void LateUpdate()
    {
     
        if (!CameraMode.Instance.IsCameraModeOn || !_gestureDetector.IsUsingGesture(Gesture.Grab))
            return;

        Vector3 rotationAxis = Vector3.up;

        if (_gestureDetector.IsHandFacingDown())
            _isHandFacingUp = -1f;
        else if (_gestureDetector.IsHandFacingUp())
            _isHandFacingUp = 1f;
        else
            _isHandFacingUp = 0f;

        //Debug.Log("wristrot: " + _gestureDetector.GetWristRotation());
      
        transform.RotateAround(_target.position, rotationAxis, _isHandFacingUp *_orbitSpeed * Time.deltaTime);

  
    }
}