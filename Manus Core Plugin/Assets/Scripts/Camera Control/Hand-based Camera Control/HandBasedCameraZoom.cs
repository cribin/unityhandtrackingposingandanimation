﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class HandBasedCameraZoom : MonoBehaviour
{

    [SerializeField] private GestureDetector _gestureDetector;
    [SerializeField] private float _maxFieldOfView = 60;
    [SerializeField] private float _minFieldOfView = 5;

    [SerializeField] private float _zoomSpeed = 5;

    private readonly float _baseZoomSpeed = 100f;
    private float _currentFieldOfView;
    private Camera _mainCamera;

    // private CameraMode _cameraMode;
    // Use this for initialization
    void Start()
    {
        _mainCamera = GetComponent<Camera>();
        _currentFieldOfView = _mainCamera.fieldOfView;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!CameraMode.Instance.IsCameraModeOn || !_gestureDetector.IsUsingGesture(Gesture.IndexPoint))
            return;

        float scroll = 0f;
        if (_gestureDetector.IsHandFacingDown())
            scroll = 1f;
        else if (_gestureDetector.IsHandFacingUp())
            scroll = -1f;

        _currentFieldOfView -= scroll * _zoomSpeed * _baseZoomSpeed * Time.deltaTime;
        _currentFieldOfView = Mathf.Clamp(_currentFieldOfView, _minFieldOfView, _maxFieldOfView);

        _mainCamera.fieldOfView = _currentFieldOfView;

    }
}
