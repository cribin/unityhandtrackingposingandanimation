﻿using System;
using Assets.Unity_Runtime_Recorder.Scripts.UnityAnimSaver;
using UnityEngine;

public class KeyFrameAnimationViewer : MonoBehaviour
{
    [SerializeField] private Animator _modelAnimator;
    [SerializeField] private MyUnityAnimationRecorder _myUnityAnimationRecorder;
    [SerializeField] private AnimationClip _defaultAnimationClip;


    private Avatar _currModelAvatar;
    private bool _playAnimation = false;

    private AnimatorOverrideController _animatorOverrideController;

    public event Action<bool> IsAnimationPlayingEvent;

    // Use this for initialization
    void Start()
    {
        if (!_modelAnimator)
        {
            Debug.LogError("KeyFrameAnimatorViewer needs the Animator Component of the model to play the Animation");
            return;
        }
        if (!_myUnityAnimationRecorder)
        {
            Debug.LogError("KeyFrameAnimatorViewer needs MyUnityAnimationRecorder Component to play the Animation");
            return;
        }

        _myUnityAnimationRecorder.NewClipCreatedEvent += OnNewClipCreated;
        _animatorOverrideController = new AnimatorOverrideController(_modelAnimator.runtimeAnimatorController);
        _modelAnimator.runtimeAnimatorController = _animatorOverrideController;
    }

    void OnDestroy()
    {
        _myUnityAnimationRecorder.NewClipCreatedEvent -= OnNewClipCreated;
    }

    public float GetCurrentAnimatorClipTime()
    {
        AnimatorStateInfo animationState = _modelAnimator.GetCurrentAnimatorStateInfo(0);
        AnimatorClipInfo[] myAnimatorClip = _modelAnimator.GetCurrentAnimatorClipInfo(0);
        float myTime = -1;

        if (myAnimatorClip.Length > 0)
        {
            float clipLength = myAnimatorClip[0].clip.length;
            myTime = (clipLength * animationState.normalizedTime) % clipLength;
        }

        return myTime;
    }

    public float GetCurrentAnimatorClipLength()
    {
        AnimatorClipInfo[] myAnimatorClip = _modelAnimator.GetCurrentAnimatorClipInfo(0);
        float clipLength = -1;

        if (myAnimatorClip.Length > 0)
            clipLength = myAnimatorClip[0].clip.length;

        return clipLength;
    }


    void OnNewClipCreated(AnimationClip newClip)
    {
        if (IsAnimationPlayingEvent != null)
            IsAnimationPlayingEvent.Invoke(true);

        _currModelAvatar = _modelAnimator.avatar;
        _modelAnimator.avatar = null;

        _animatorOverrideController[_defaultAnimationClip] = newClip;
        _modelAnimator.SetBool("playAnimation", true);
    }

    void OnAnimationPlaybackStopped()
    {
        _modelAnimator.SetBool("playAnimation", false);
        _animatorOverrideController[_defaultAnimationClip] = _defaultAnimationClip;
        _modelAnimator.avatar = _currModelAvatar;


        if (IsAnimationPlayingEvent != null)
            IsAnimationPlayingEvent.Invoke(false);
    }

    public void OnPlayAndStopAnimationActivated()
    {
        _playAnimation = !_playAnimation;
        if (_playAnimation)
            _myUnityAnimationRecorder.SaveCurrentRecording();
        else
            OnAnimationPlaybackStopped();
    }
}