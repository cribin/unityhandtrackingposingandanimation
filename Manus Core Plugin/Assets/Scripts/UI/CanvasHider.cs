﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHider : MonoBehaviour
{
    [SerializeField] private CanvasGroup[] _canvasGroups = new CanvasGroup[2];
    private bool _hideUI = false;

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(InputKey.SwitchUITransparency)))
            return;

        _hideUI = !_hideUI;
        SetUIAlpha(_hideUI ? 0f : 1f);
    }

    void SetUIAlpha(float alpha)
    {
        foreach (var canvasGroup in _canvasGroups)
        {
            canvasGroup.alpha = alpha;
        }
    }
}