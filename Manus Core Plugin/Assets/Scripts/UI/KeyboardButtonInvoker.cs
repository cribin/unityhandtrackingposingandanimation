﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class KeyboardButtonInvoker : MonoBehaviour
{
    [SerializeField] private InputKey _inputKey = InputKey.None;

    private Button _button;

    void Start()
    {
        _button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_button.interactable && Input.GetKeyDown(InputKeyManager.Instance.GetKeyCode(_inputKey)))
            _button.onClick.Invoke();
    }
}