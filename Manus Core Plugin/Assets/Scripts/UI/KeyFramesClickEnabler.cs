﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyFramesClickEnabler : MonoBehaviour
{
    [SerializeField] private KeyFrameAnimationViewer _keyFrameAnimationViewer;

    private CanvasGroup _canvasGroup;

    // Use this for initialization
    void Start()
    {
        if (_keyFrameAnimationViewer == null)
        {
            Debug.LogError("Error KeyFramesClickEnabler needs a reference to the KeyFrameAnimationViewer!");
            return;
        }
        _canvasGroup = GetComponent<CanvasGroup>();

        if (_canvasGroup == null)
        {
            Debug.LogError("Error KeyFramesClickEnabler needs to have a CanvasGroup Component!");
        }

        _keyFrameAnimationViewer.IsAnimationPlayingEvent += OnAnimationPlaybackStateChanged;
    }

    void OnDestroy()
    {
        _keyFrameAnimationViewer.IsAnimationPlayingEvent -= OnAnimationPlaybackStateChanged;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnAnimationPlaybackStateChanged(bool isAnimationPlaying)
    {
        _canvasGroup.blocksRaycasts = !isAnimationPlaying;
    }
}