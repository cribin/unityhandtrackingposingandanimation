﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderTimeLineCreator : MonoBehaviour
{
    private Slider _slider;

    [SerializeField] private float _sliderMaxValue = 2;
    [SerializeField] private GameObject _timeLineObject;
    [SerializeField] private RectTransform _handleArea;
    [SerializeField] private RectTransform _GUIArea;

    [SerializeField] private float _deltaTime = 0.025f;

    private Vector2 _timeLineOffset = new Vector2(0f, 5f);
    private float _verticalOffset = 8f;

    private Vector2 _textAnchorOffset;

    // Use this for initialization
    void Start()
    {
        _slider = GetComponent<Slider>();
        if (_slider == null)
        {
            Debug.LogError("SliderTimeLineCreator needs a Slider component!");
            return;
        }

        _slider.maxValue = _sliderMaxValue;
        _textAnchorOffset = new Vector2(10, _timeLineOffset.y + 7);
        InstantiateTimeLineUI();
    }

    void InstantiateTimeLineUI()
    {
        // float deltaTime = (_deltaTime - _slider.minValue) / (_slider.maxValue - _slider.minValue);
        for (double i = 0; i < 1.01; i += _deltaTime)
        {
            double sliderValue = _slider.minValue + i * (_slider.maxValue - _slider.minValue);

            RectTransform verticalTimeLineRectTransform =
                InitAndParentPrefabInTimeline("Prefabs/VerticalTimeLine", _handleArea);
            AnchorRectTransformInTimeLine(verticalTimeLineRectTransform, _timeLineOffset, (float) i);

            if (Math.Abs(i % 0.1f) < 0.001)
            {
                RectTransform timeStampRectTransform =
                    InitAndParentPrefabInTimeline("Prefabs/TimeStampText", _handleArea);
                AnchorRectTransformInTimeLine(timeStampRectTransform, _textAnchorOffset, (float)i);
                TextMeshProUGUI timeStampText = timeStampRectTransform.GetComponent<TextMeshProUGUI>();
                timeStampText.text = ((float)sliderValue).ToString(CultureInfo.InvariantCulture);

                verticalTimeLineRectTransform.offsetMax = new Vector2(verticalTimeLineRectTransform.offsetMax.x,
                    verticalTimeLineRectTransform.offsetMax.y + _verticalOffset);
            }
        }
    }

    RectTransform InitAndParentPrefabInTimeline(string prefabPath, RectTransform parentTransform)
    {
        GameObject customPrefab = Instantiate(Resources.Load(prefabPath) as GameObject);

        RectTransform rectTransform = customPrefab.GetComponent<RectTransform>();
        rectTransform.SetParent(parentTransform, false);
       // rectTransform.SetParent(_GUIArea, false);

        return rectTransform;
    }

    void AnchorRectTransformInTimeLine(RectTransform rectTransform, Vector2 anchorOffset,
        float anchorXPos)
    {
        rectTransform.anchoredPosition = new Vector2(anchorOffset.x, anchorOffset.y);
        rectTransform.anchorMin = new Vector2(anchorXPos, 0);
        rectTransform.anchorMax = new Vector2(anchorXPos, 1);
    }
}