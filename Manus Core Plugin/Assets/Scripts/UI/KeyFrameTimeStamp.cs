﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyFrameTimeStamp : MonoBehaviour, IPointerClickHandler, IPointerDownHandler
{
    [SerializeField] private RectTransform _rightClickOptions;
    [SerializeField] private RawImage _screenShotArea;
    private KeyFrameTimeStampManager _keyFrameTimeStampManager;
    private RectTransform _rectTransform;
    private Image _image;
    public float TimeStamp { get; private set; }

    public Texture2D KeyFrameScreenshot { get; set; }

    private readonly Color _activeColor = Color.green;
    private readonly Color _inactiveColor = Color.red;
    private readonly Color _modifiedColor = Color.yellow;

    public event Action<KeyFrameTimeStamp> ModifyTimeStampEvent;
    public event Action<KeyFrameTimeStamp> DeleteTimeStampEvent;

    private int _timeStampIndex = -1;
    // Use this for initialization
    void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        if (_rectTransform == null)
            Debug.LogError("KeyFrameTimeStamp must have a RectTransform Component");

        _image = GetComponent<Image>();
        if(_image == null)
            Debug.LogError("KeyFrameTimeStamp must have an Image Component");
    }

    /// <summary>
    /// This method should be called onyl once
    /// </summary>
    /// <param name="timeStampIndex"></param>
    /// <param name="parentSlider"></param>
    /// <param name="parentRectTransform"></param>
    public void ActivateKeyFrame(int timeStampIndex, Slider parentSlider, RectTransform parentRectTransform)
    {
        if (_timeStampIndex >= 0)
            return;
        _timeStampIndex = timeStampIndex;
      
        ParentTimeStampToSlider(parentSlider, parentRectTransform, false);
        _image.color = _activeColor;
    }

    public void ModifyKeyFrame(Slider parentSlider, RectTransform parentRectTransform)
    {
        if (_image.color != _inactiveColor)
            return;

        parentSlider.value = TimeStamp;
        ParentTimeStampToSlider(parentSlider, parentRectTransform, true);
        _image.color = _modifiedColor;
    }

    public void ReactivateKeyFrame(Slider parentSlider)
    {
        parentSlider.value = TimeStamp;
        parentSlider.handleRect = _rectTransform;
    }

    public void DeactivateKeyFrame(Slider parentSlider, RectTransform parentRectTransform)
    {
        if (_image.color == _inactiveColor)
            return;

        UnParentTimeStampToSlider(parentSlider, parentRectTransform);
        _image.color = _inactiveColor;

        ScreenShotHandler.Instance.TakeFullScreenShot(500, 500, this);

    }

    void ParentTimeStampToSlider(Slider parentSlider, RectTransform parentRectTransform, bool worldPositionStays)
    {
        _rectTransform.SetParent(parentRectTransform, worldPositionStays);
        parentSlider.handleRect = _rectTransform;
        parentSlider.onValueChanged.AddListener(OnSliderValueChanged);
    }

    void UnParentTimeStampToSlider(Slider parentSlider, RectTransform parentRectTransform)
    {
        parentSlider.onValueChanged.RemoveListener(OnSliderValueChanged);
        parentSlider.handleRect = null;
        _rectTransform.SetParent(parentRectTransform);
    }

    void OnSliderValueChanged(float newValue)
    {
        TimeStamp = newValue;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_image.color != _inactiveColor)
            return;

        if (eventData.button == PointerEventData.InputButton.Right)
        {
            _rightClickOptions.gameObject.SetActive(!_rightClickOptions.gameObject.activeSelf);
            _screenShotArea.gameObject.SetActive(!_screenShotArea.gameObject.activeSelf);
            if(_screenShotArea.gameObject.activeSelf)
                _screenShotArea.texture = KeyFrameScreenshot;
        }
    }

    void SendModifyRequestToManager()
    {
        if (_image.color == _activeColor)
            return;

        _rightClickOptions.gameObject.SetActive(false);

        if (ModifyTimeStampEvent != null)
            ModifyTimeStampEvent.Invoke(this);
    }

    public void OnDeleteButtonPressed()
    {
        if (_image.color == _activeColor)
            return;

        if (DeleteTimeStampEvent != null)
            DeleteTimeStampEvent.Invoke(this);
    }


    public void DeleteKeyFrame()
    {
        if (_image.color != _inactiveColor)
            return;
      
        Destroy(gameObject);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_image.color != _inactiveColor)
            return;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            SendModifyRequestToManager();
        }
    }
}