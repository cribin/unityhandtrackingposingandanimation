﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyFrameTimeStampManager : MonoBehaviour
{
    [SerializeField] private RectTransform _capturedParentRectTransform;
    private List<KeyFrameTimeStamp> _keyFrameTimeStamps = new List<KeyFrameTimeStamp>();
    private RectTransform _rectTransform;
    private Slider _parentSlider;

    private int _activeTimeStampIndex = -1;
    //private bool _modifiedTimeStamp = false;

    public event Action<float> NewFrameCapturedEvent;

    public event Action<int, float> FrameModifiedEvent;

    public event Action<int> FrameDeletedEvent;

    private KeyFrameTimeStamp _modifiedKeyFrameTimeStamp = null;

    // Use this for initialization
    void Awake()
    {
        if (_capturedParentRectTransform == null)
        {
            Debug.LogError("KeyFrameTimeStampManager needs a RectTransform to hold captured frames!");
            return;
        }

        _rectTransform = GetComponent<RectTransform>();
        if (_rectTransform == null)
        {
            Debug.LogError("KeyFrameTimeStampManager needs a RectTransform!");
            return;
        }

        Component[] sliderComponents = GetComponentsInParent(typeof(Slider));

        if (sliderComponents == null)
        {
            Debug.LogError("KeyFrameTimeStampManager must have a Slider Component as parent");
            return;
        }

        _parentSlider = (Slider) sliderComponents[0];
        InstantiateKeyFrameTimeStamp();
    }

    void OnDestroy()
    {
        foreach (var timeStamp in _keyFrameTimeStamps)
        {
            timeStamp.ModifyTimeStampEvent -= OnTimeStampModify;
            timeStamp.DeleteTimeStampEvent -= OnTimeStampDelete;
        }
    }

    void InstantiateKeyFrameTimeStamp()
    {
        //We can't create a timestamp if another is already active
        if (_activeTimeStampIndex >= 0)
            return;

        GameObject keyFrameTimeStampObject = Instantiate(Resources.Load("Prefabs/KeyFrameTimeStamp") as GameObject);
        KeyFrameTimeStamp keyFrameTimeStamp = keyFrameTimeStampObject.GetComponent<KeyFrameTimeStamp>();
        _keyFrameTimeStamps.Add(keyFrameTimeStamp);
        _activeTimeStampIndex = _keyFrameTimeStamps.Count - 1;
        _parentSlider.value = 0;
        keyFrameTimeStamp.ActivateKeyFrame(_activeTimeStampIndex, _parentSlider, _rectTransform);
        keyFrameTimeStamp.ModifyTimeStampEvent += OnTimeStampModify;
        keyFrameTimeStamp.DeleteTimeStampEvent += OnTimeStampDelete;
    }

    /// <summary>
    /// This method is called when the capture keyframe button is pressed or the corresponding key is pressed.
    /// </summary>
    public void CaptureNewFrame()
    {
        if (_activeTimeStampIndex < 0 || _modifiedKeyFrameTimeStamp != null)
            return;

        if (KeyFrameWithSameTimeStampExist())
        {
            Debug.Log("Key-frame capture failed! There already exists a key-frame at the specified time");
            return;
        }

        if (NewFrameCapturedEvent != null)
            NewFrameCapturedEvent.Invoke(_keyFrameTimeStamps[_activeTimeStampIndex].TimeStamp);

        DeactivateCurrentKeyFrameTimeStamp();
        InstantiateKeyFrameTimeStamp();
    }

    bool KeyFrameWithSameTimeStampExist()
    {
        for (int i = 0; i < _keyFrameTimeStamps.Count; i++)
        {
            if (i == _activeTimeStampIndex)
            {
                //Debug.Log("Active timestamp: " + _activeTimeStampIndex + " " + _keyFrameTimeStamps[_activeTimeStampIndex].TimeStamp);
                continue;
            }
            //Debug.Log("other timestamps:"+  i + " " +  _keyFrameTimeStamps[i].TimeStamp);
            if (Math.Abs(_keyFrameTimeStamps[i].TimeStamp -
                         _keyFrameTimeStamps[_activeTimeStampIndex].TimeStamp) < 0.01)
                return true;
        }
     

        return false;
    }

    /// <summary>
    /// This method is directly called from the slider component
    /// </summary>
    public void OnPointerEndDrag()
    {
        if (_modifiedKeyFrameTimeStamp != null)
        {
            _modifiedKeyFrameTimeStamp = null;
            if (FrameModifiedEvent != null)
                FrameModifiedEvent.Invoke(_activeTimeStampIndex, _keyFrameTimeStamps[_activeTimeStampIndex].TimeStamp);

            DeactivateCurrentKeyFrameTimeStamp();
            _activeTimeStampIndex = _keyFrameTimeStamps.Count - 1;
            _keyFrameTimeStamps[_activeTimeStampIndex].gameObject.SetActive(true);
            _keyFrameTimeStamps[_activeTimeStampIndex].ReactivateKeyFrame(_parentSlider);
        }
    }

    void DeactivateCurrentKeyFrameTimeStamp()
    {
        if (_activeTimeStampIndex < 0)
            return;

        _keyFrameTimeStamps[_activeTimeStampIndex].DeactivateKeyFrame(_parentSlider, _capturedParentRectTransform);

        _activeTimeStampIndex = -1;
    }

    /// <summary>
    /// Deactivates current keyframes and reactivates keyframe to be modified
    /// </summary>
    /// <param name="keyFrameTimeStamp"></param>
    void OnTimeStampModify(KeyFrameTimeStamp keyFrameTimeStamp)
    {
        if (_modifiedKeyFrameTimeStamp != null)
            return;
        _modifiedKeyFrameTimeStamp = keyFrameTimeStamp;
        int timeStampIndex = _keyFrameTimeStamps.IndexOf(_modifiedKeyFrameTimeStamp);
        _keyFrameTimeStamps[_activeTimeStampIndex].gameObject.SetActive(false);
        _activeTimeStampIndex = timeStampIndex;
        _keyFrameTimeStamps[_activeTimeStampIndex].ModifyKeyFrame(_parentSlider, _rectTransform);
    }

    void OnTimeStampDelete(KeyFrameTimeStamp keyFrameTimeStamp)
    {
        int timeStampIndex = _keyFrameTimeStamps.IndexOf(keyFrameTimeStamp);

        if (FrameDeletedEvent != null)
            FrameDeletedEvent.Invoke(timeStampIndex);


        _keyFrameTimeStamps[timeStampIndex].DeleteKeyFrame();
        _keyFrameTimeStamps.RemoveAt(timeStampIndex);
        if (_modifiedKeyFrameTimeStamp != null)
            _activeTimeStampIndex = _keyFrameTimeStamps.IndexOf(_modifiedKeyFrameTimeStamp);
        else
            _activeTimeStampIndex = _keyFrameTimeStamps.Count - 1;
    }
}