﻿using UnityEngine;
using UnityEngine.UI;

public class SliderUIActivator : MonoBehaviour
{
    [SerializeField] private Slider _slider;

    [SerializeField] private GameObject _capturedKeyFrames;

    // Update is called once per frame
    void Update()
    {
        _slider.gameObject.SetActive(!CameraMode.Instance.IsCameraModeOn);
        _capturedKeyFrames.SetActive(!CameraMode.Instance.IsCameraModeOn);
    }
}