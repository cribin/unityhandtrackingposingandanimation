﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

[RequireComponent(typeof(Button)), RequireComponent(typeof(VoiceCapture))]
public class VoiceCaptureButtonInvoker : MonoBehaviour
{
    private Button _button;
    private VoiceCapture _voiceCapture;

    void Awake()
    {
        _button = GetComponent<Button>();
        _voiceCapture = GetComponent<VoiceCapture>();
    }

    void OnEnable()
    {
        _voiceCapture.KeywordRecognizedEvent += OnKeywordRecognized;
    }

    void OnDisable()
    {
        _voiceCapture.KeywordRecognizedEvent -= OnKeywordRecognized;
    }

    private void OnKeywordRecognized(PhraseRecognizedEventArgs args)
    {
        if (_button.interactable)
            _button.onClick.Invoke();
    }
}