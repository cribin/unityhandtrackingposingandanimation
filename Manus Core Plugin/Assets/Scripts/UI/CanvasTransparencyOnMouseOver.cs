﻿using UnityEngine;

public class CanvasTransparencyOnMouseOver : MonoBehaviour
{

    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private float _baseAlphaValue = 0.1f;

    void OnMouseOver()
    {
        Debug.Log("Mouse over");
        _canvasGroup.alpha = 1;
    }


    void OnMouseExit()
    {

        _canvasGroup.alpha = _baseAlphaValue;
    }
}