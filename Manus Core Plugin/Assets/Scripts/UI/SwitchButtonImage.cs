﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SwitchButtonImage : MonoBehaviour
{
    [SerializeField] private Sprite[] _buttonSwitchSprites;

    private int _currentSpriteIndex;

    private Button _button;


    // Use this for initialization
    void Start()
    {
        _button = GetComponent<Button>();
        _button.image.sprite = _buttonSwitchSprites[_currentSpriteIndex];
    }

    public void SwitchImage()
    {
        if (_currentSpriteIndex >= _buttonSwitchSprites.Length - 1)
            _currentSpriteIndex = 0;
        else
            _currentSpriteIndex++;
        
        _button.image.sprite = _buttonSwitchSprites[_currentSpriteIndex];
    }
}