﻿using UnityEngine;
using UnityEngine.UI;

public class AnimationPlaybackLine : MonoBehaviour
{
    [SerializeField] private KeyFrameAnimationViewer _keyFrameAnimationViewer;
    [SerializeField] private Slider _parentSlider;
    private RectTransform _rectTransform;
    private Image _playbackLineImage;

    // Use this for initialization
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _playbackLineImage = GetComponent<Image>();
        _playbackLineImage.enabled = false;
        _keyFrameAnimationViewer.IsAnimationPlayingEvent += OnAnimationPlaybackStateChanged;
    }

    void OnDestroy()
    {
        _keyFrameAnimationViewer.IsAnimationPlayingEvent -= OnAnimationPlaybackStateChanged;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_playbackLineImage.enabled)
            return;

        float currentClipTime = _keyFrameAnimationViewer.GetCurrentAnimatorClipTime();
        if (!(currentClipTime > -1)) return;
       
        currentClipTime = (currentClipTime -_parentSlider.minValue) / (_parentSlider.maxValue - _parentSlider.minValue);
        SetPlaybackLinePos(currentClipTime);
    }

    void OnAnimationPlaybackStateChanged(bool isAnimationPlaying)
    {
        _playbackLineImage.enabled = isAnimationPlaying;
    }

    void SetPlaybackLinePos(float anchorXPos)
    {
        _rectTransform.anchoredPosition = new Vector2(0f, 5f);
        _rectTransform.anchorMin = new Vector2(anchorXPos, 0);
        _rectTransform.anchorMax = new Vector2(anchorXPos, 1);
    }
}