﻿using UnityEngine;
using UnityEngine.UI;

public class UserButtonsManager : MonoBehaviour
{
    [SerializeField] private string _cameraModeButtonName = "SwitchCameraModeButton";
    [SerializeField] private string _playPauseButtonName = "PlayAndStopAnimationButton";
    private Button[] _childButtons;

    private int _playAndStopAnimationButtonIndex = -1;
    private int _cameraModeButtonIndex = -1;

    // Use this for initialization
    void Start()
    {
        _childButtons = new Button[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            _childButtons[i] = transform.GetChild(i).GetComponent<Button>();
            if (_childButtons[i].name == _playPauseButtonName)
            {
                _childButtons[i].onClick.AddListener(OnPlayAndStopButtonClicked);
                _playAndStopAnimationButtonIndex = i;
            }
            if (_childButtons[i].name == _cameraModeButtonName)
            {
                _childButtons[i].onClick.AddListener(OnCameraModeButtonClicked);
                _cameraModeButtonIndex = i;
            }
        }
    }

    void OnDestroy()
    {
        _childButtons[_playAndStopAnimationButtonIndex].onClick.RemoveAllListeners();
    }

    public void OnPlayAndStopButtonClicked()
    {
        for (int i = 0; i < _childButtons.Length; i++)
        {
            if(i == _playAndStopAnimationButtonIndex)
                continue;

            _childButtons[i].interactable = !_childButtons[i].interactable;
        }
    }

    public void OnCameraModeButtonClicked()
    {
        for (int i = 0; i < _childButtons.Length; i++)
        {
            if (i == _cameraModeButtonIndex)
                continue;

            _childButtons[i].interactable = !_childButtons[i].interactable;
        }
    }
}